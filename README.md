# Verteilte Systeme++

## Delivery of solutions
All homework's have to be handed in using the OLAT system. Deadline is day before the exercise (Thursday) at 16:00, to allow me to look over your solutions. The solution handed in should consist of two (2) files only:

A txt or pdf file explaining what you did, your results, your problems (including your name, points achieved for the sheet, how to run your developed application)

A archive in .zip or .tar.gz format including all files needed for running the program, excluding files provided by the course and excluding files generated in the exercise (png, gif, debug.out, ...)

Failure to hand in the solutions correctly (missing name, docx, 7z files, more or less then 2 files...) will result in points reduction! 60% of all points must be reached for a positive grade. Blackboard grades  normally +-1 the overall grade, except if they are negative. Then the resulting course grad is automatically negative as well.

For the first exercise there was no homework except of signing up for AWS and requesting a Austrian Grid certificate! No hand over needed for this task.
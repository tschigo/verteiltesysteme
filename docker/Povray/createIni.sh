#!/bin/bash

if [ "$#" -lt 3 ]
then
  echo "Set frames and instances"
  exit 1;
fi


#frames
M=$1

#instances
N=$2


# calc ini files
fpp=$((M/N))

START="1"

# create ini files
while [ $START -le $N ]
do
    F_START=$[F_END+1]
  if [ $((M%N)) -ge $START ]; then
    F_END=$[$F_END+$fpp+1]
  else
    F_END=$[$F_END+$fpp]
  fi

  if [ $START -eq $3 ]; then
    echo "Antialias=Off" >> frame.ini
    echo "Antialias_Threshold=0.1" >> frame.ini
    echo "Antialias_Depth=2" >> frame.ini
    echo "Initial_Clock=0" >> frame.ini
    echo "Final_Clock=1" >> frame.ini
    echo "Cyclic_Animation=on" >> frame.ini
    echo "Pause_when_Done=off" >> frame.ini
    echo "Initial_Frame=1" >> frame.ini
    echo "Final_Frame="$M >> frame.ini
    echo "Subset_Start_Frame="$F_START >> frame.ini
    echo "Subset_End_Frame="$F_END >> frame.ini
  fi

  START=$[$START+1]
done

#!/bin/bash


# $1 frames
# $2 instances
# $3 current instance
# $4 pov file


if [ "$#" -gt 4 ]
then
  echo "[ERROR] not enough argument"
fi

./home/createIni.sh $1 $2 $3
mv frame.ini /home 

./home/povray /home/frame.ini +I /home/pov/$4 +Oframe.png +FN +W1024 +H768
mv *.png /home/

./home/gm convert -loop 0 -delay 0 /home/*.png res_$3.gif
wait
mv res_$3.gif /home/share



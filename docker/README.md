
# Docker
Docker is an open source project to pack, ship and run any application as a lightweight container.

Docker containers are both hardware-agnostic and platform-agnostic. This means they can run anywhere, from your laptop to the largest EC2 compute instance and everything in between - and they don't require you to use a particular language, framework or packaging system. That makes them great building blocks for deploying and scaling web apps, databases, and backend services without depending on a particular stack or provider.

Docker began as an open-source implementation of the deployment engine which powers dotCloud, a popular Platform-as-a-Service. It benefits directly from the experience accumulated over several years of large-scale operation and support of hundreds of thousands of applications and databases.

[from https://github.com/docker/docker]





## Idea

Main:

- remote call to initiate
- give number of instances
- scene to render

EC2:

- s3 mount
- images to render
- install docker stuff


Docker:

- create docker image containing povray and gm
- docker image should contain scripts to run computation


## Installing Docker

	sudo yum update -y
	sudo yum install -y docker
	sudo service docker start
	sudo usermod -a -G docker ec2-user

### Dockerkfile

	FROM debian:stable 
	MAINTAINER Lukas, Patrick and Michael
	RUN mkdir /home/share
	COPY Povray home


The docker container is hosted on [docker](http://docker.com) at [docker/tschigo/povray_gm](https://registry.hub.docker.com/u/tschigo/povray_gm/)


### Docker with gm and povray

The docker contains a script `exec.sh` which runs povray and and gm and returns a gif. 

	docker run -v /home/ec2-user/tmp:/home/share tschigo/povray_gm ./home/exec.sh $4 $5 $6 $7
	
with `docker run tschigo/povray_gm` we start the docker. It is hostet on docker by the user tschigo and the container is called povray_gm. `-v /home/ec2-user/tmp:home/share` allows us to mount in docker a folder where we are able to share files between the to parties.
At the end we will save the file on the ec2 instance.

### main.sh 
`main.sh` script starts a given number of instances, mount S3 bucket and starts docker with povray and gm.
When the scirpt is finished it saves the final gif on your Desktop.


#GUI 

![frontend.png](frontend.png =x200)
We created an graphical user interface that is able to configure the process execution, upload configuration files. Show the status and finally download the resulting GIF.

For our method the user credentials need to be configured very carefully and due to the fact that our front-end access key user is designed to be different than the main user we need to configure the instances and s3fs and S3 accordingly. This was very challenging as s3fs still has severe problems with our kind of mounting and this forced us to simplify everything for use with one single "mighty" user.

S3 needs a CORS (Cross origin requests specification) configuration to allow Put/Get/List Operations from an authenticated user. This configuration process is not error prone and difficult to handle.

## Frontend Angular-JS with AWS JS-SDK
The Frontend is based on Cheyne Wallace's implementation of an Uploader to S3. We use AWS JS SDK to connect to the bucket and upload using the user provided credentials and bucket name.

For the backend and script execution we created an RESTFUL API using NodeJS.

The AWS SDK features are not fully featured, as the EC2 module was developed in April 2015. Therefore not all functions could be made from AngularJS. Describe running instances calls directly AWS Services to provide information.

An interesting fact is, that the response in the AWS JS SDK is provided as XML and capsuled in an object which does not provide accessors to all fields. For example describe instances returns the complete set of information but only few values of the object have documented accessors available. It would be neccessary to write a separate parser for this on ourselves.

Our implementation is responsive and able to run on mobile devices as well and we feature a **Toastr** notification service, which shows errors and results of our calls.

It is difficult to visualize the List of S3 Objects.

## Backend for Execution
Our shell scripts which start the instances and configure docker are controlled via an RESTFUL API backend. The implementation was made in NodeJS as this allows us to use asynchronous behaviour.

The NodeJS uses **shelljs** library to execute our scripts. The parameters are provided in the query URL. Therefore at the moment it is not secure to run the application on any public network as the API calls are unencrypted.

Some difficulties lie in the use of shell scripts and providing correct key files. The adaption to our three uses was planned to be on the fly, but anyway this does not always work.

The general workflow is able to be executed through our scripts controlled by the user interface. It is interesting to extend the program when the feature set of the sdk grows and Amazon supports JSON instead of XML in their JS SDK.


#ECS approach

This approach focuses on the usage of a big cluster which runs a variable amount of dockers.
Our previous approach takes a defined number of instances where dockers are distributed manually. 
This leads to an exponential problem when using a cluster with n instances (which can be variable or auto-scaled) that run m docker containers.
Scheduling, scaling and error/state handling becomes problematic when doing by hand.

AWS offers a service called EC2 Container Service (ECS) which manages the scheduling of jobs in a cluster and offers information during the execution process.
To use ECS you need to define a cluster. For this cluster you can assign EC2 instances using an ECS agent.
To run docker containers you need to define tasks for the cluster. The task definition is written in JSON format, defined by AWS.
It mainly represents the different parameters used in the `docker run` command.

For this approach, we created a docker image, which includes povray, gm, an execution script and s3fs to mount an s3 bucket inside the container.
Afterwards, the task definition was created, with the needed parameters for execution. Here we defined environment variables with the AWS credentials used for s3 and the command to execute the script, which followingly executes povray and gm, as well as the docker image to use.

At this point we have a cluster of n instances and a task definition. In the ECS console or the ECS CLI we now can define how many tasks (dockers) we want to run. ECS then schedules that amount to the available resources (EC2 instances in the cluster).

Problem:
To mount s3 inside a docker, we have to run the docker in privileged mode. Sadly, the ECS task definition does NOT support the privileged mode for dockers, yet. Hence, using ECS for this exercise does not work.

'use strict';

var controllers = angular.module('controllers', []);

controllers.controller('UploadController',['$scope', 'apiwrapper', function($scope, apiwrapper) {
  $scope.sizeLimit      = 10585760; // 10MB in Bytes
  $scope.uploadProgress = 0;
  $scope.creds          = {};

  $scope.upload = function() {
    AWS.config.update({ accessKeyId: $scope.creds.access_key, secretAccessKey: $scope.creds.secret_key });
    AWS.config.region = 'us-west-1';
    var bucket = new AWS.S3({ params: { Bucket: $scope.creds.bucket } });

    if($scope.file) {
        // Perform File Size Check First
        var fileSize = Math.round(parseInt($scope.file.size));
        if (fileSize > $scope.sizeLimit) {
          toastr.error('Sorry, your attachment is too big. <br/> Maximum '  + $scope.fileSizeLabel() + ' file attachment allowed','File Too Large');
          return false;
        }
        // Prepend Unique String To Prevent Overwrites $scope.uniqueString() + '-' +
        var uniqueFileName =  $scope.file.name;

        var params = { Key: uniqueFileName, 
                       ContentType: $scope.file.type, 
                       Body: $scope.file, 
                       ServerSideEncryption: 'AES256',
                       ACL: 'public-read-write',
                       //GrantReadACP: 'ec2-user' 
                     };

        bucket.putObject(params, function(err, data) {
          if(err) {
            toastr.error(err.message,err.code);
            return false;
          }
          else {
            // Upload Successfully Finished
            toastr.success('File Uploaded Successfully', 'Done');

            // Reset The Progress Bar
            setTimeout(function() {
              $scope.uploadProgress = 0;
              $scope.$digest();
            }, 4000);
          }
        })
        .on('httpUploadProgress',function(progress) {
          $scope.uploadProgress = Math.round(progress.loaded / progress.total * 100);
          $scope.$digest();
        });
      }
      else {
        // No File Selected
        toastr.error('Please select a file to upload');
      }
    }

    $scope.fileSizeLabel = function() {
    // Convert Bytes To MB
    return Math.round($scope.sizeLimit / 1024 / 1024) + 'MB';
  };

  $scope.uniqueString = function() {
    var text     = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 8; i++ ) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  };

  $scope.describeInstances = function(){
    // $scope.creds.secret_key 
    AWS.config.update({ accessKeyId: $scope.creds.access_key, secretAccessKey: $scope.creds.secret_key  });
    
    AWS.config.region = 'us-west-1';
    

    var ec2 = new AWS.EC2();
    var params = {
        DryRun: false, //true || 
        //Filters: [
        //  {
        //    Name: 'STRING_VALUE',
        //    Values: [
        //      'STRING_VALUE',
        //      /* more items */
        //    ]
        //  },
          /* more items */
        //],
        //InstanceIds: [
        //  'STRING_VALUE',
          /* more items */
        //],
        MaxResults: 10,
        //NextToken: 'STRING_VALUE'
    };
    ec2.describeInstances(params, function(err, data) {
      if (err) toastr.error(err, err.stack); // an error occurred
      else{
          for(var reservation in data.Reservations) {
            for(var instance in data.Reservations[reservation].Instances) {
            toastr.success(
              data.Reservations[reservation].Instances[instance].State.Name,  
            data.Reservations[reservation].Instances[instance].State.Name);           // successful response
              }
            }
          }
    });
  };

  $scope.createInstances = function(){
    // 
    AWS.config.update({ accessKeyId: $scope.creds.access_key, secretAccessKey: $scope.creds.secret_key  });
    
    AWS.config.region = 'us-west-1';
    

    var ec2 = new AWS.EC2();
    var params = {
        DryRun: false, //true || 
        //Filters: [
        //  {
        //    Name: 'STRING_VALUE',
        //    Values: [
        //      'STRING_VALUE',
        //      /* more items */
        //    ]
        //  },
          /* more items */
        //],
        //InstanceIds: [
        //  'STRING_VALUE',
          /* more items */
        //],
        MaxResults: 10,
        //NextToken: 'STRING_VALUE'
    };
    ec2.describeInstances(params, function(err, data) {
      if (err) toastr.error(err, err.stack); // an error occurred
      else{
          for(var reservation in data.Reservations) {
            for(var instance in data.Reservations[reservation].Instances) {
            toastr.success(
              data.Reservations[reservation].Instances[instance].State.Name,  
            data.Reservations[reservation].Instances[instance].State.Name);           // successful response
              }
            }
          }
    });
  };

  $scope.execute=function(){
    /*if($scope.creds.access_key==null || $scope.creds.secret_key ==null
      || $scope.creds.bucketname == null || $scope.creds.frames==null || $scope.creds.instances 
      || $scope.file==null){
      toastr.error('Missing input','fill out all fields');
    }
    else{*/
    var params = {AWSAccessKeyId: $scope.creds.access_key,
                      AWSSecretKey:  $scope.creds.secret_key,
                      bucketname:  $scope.creds.bucket,
                      frames:  $scope.creds.frames,
                      instances:  $scope.creds.instances,
                      povfile:  $scope.file.name}
    var temp = apiwrapper.get(params);
    if(temp != null){
        toastr.info('Started Execution', temp.toString());
      }
    //}
  }

  $scope.showFiles= function(){
    // 
    AWS.config.update({ accessKeyId: $scope.creds.access_key, secretAccessKey: $scope.creds.secret_key });
    AWS.config.region = 'us-west-1';
    var bucket = new AWS.S3({ params: { Bucket:  $scope.creds.bucket} });
    //

    var params = {
      Bucket: $scope.creds.bucket /* required */
      //Delimiter: 'STRING_VALUE',
      //EncodingType: 'url',
      //Marker: 'STRING_VALUE',
      //MaxKeys: 0,
      //Prefix: 'STRING_VALUE'
    }

    //toastr.success('hello');
    bucket.listObjects(params, function(err, data){
      if(err) {
        toastr.error(err.message,err.code);
        return false;
      }
      $scope.s3list = data.toString();
      toastr.success('no Error');

    });
  }
  $scope.terminateInstances = function(){
    apiwrapper.stopInstances();
    toastr.info("Instances are stopping");
  }


}]);



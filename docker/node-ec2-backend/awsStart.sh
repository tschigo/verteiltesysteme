#!/bin/bash

INSTANCES=1
KEY_NAME="bisaknosp-us-west-1"
SECURITY_GROUP="NidoranSecurityGroup"
AWS_TYPE="ami-5311e517"
INSTANCE_TYPE="t2.micro"

if [ "$#" -gt 0 ]
then
  INSTANCES=$1
fi


aws ec2 run-instances --image-id  $AWS_TYPE --count $INSTANCES --instance-type $INSTANCE_TYPE --key-name $KEY_NAME --security-groups $SECURITY_GROUP


count=0;
while ((count < $INSTANCES)); do
  count=($(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" --query "Reservations[*].Instances[*].{IP:PublicIpAddress}" --output text | wc -l))
  echo $count "instances found!";
  sleep 2;
done

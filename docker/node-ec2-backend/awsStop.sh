#!/bin/bash

array=($(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" --query "Reservations[0].Instances[*].{ID:InstanceId}" --output text))
for i in "${array[@]}"
do
  aws ec2 terminate-instances --instance-ids $i
done


#!/bin/bash

# $1 ... AWSAccessKeyId
# $2 ... AWSSecretKey
# $3 ... S3-bucket
# $4 ... frames
# $5 ... instances
# $6 ... current instance
# $7 ... pov file


# mount s3 bucket
echo $1:$2 > ~/.passwd-s3fs
chmod 400 ~/.passwd-s3fs

sudo echo "user_allow_other" > /etc/fuse.conf
s3fs $3 -o allow_other ~/mnt/ 
mount

# move pov file to docker
sudo chmod 0777 mnt/*
cp mnt/*.pov tmp/

# run docker
docker run -v /home/ec2-user/tmp:/home/share tschigo/povray_gm ./home/exec.sh $4 $5 $6 $7



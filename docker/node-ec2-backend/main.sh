#!/bin/bash

# $1 ... AWSAccessKeyId
# $2 ... AWSSecretKey
# $3 ... S3-bucket
# $4 ... frames
# $5 ... instances
# $6 ... pov file

KEY="/Users/lukas/.ssh/bisaknosp-us-west-1.pem"

if [ "$#" -lt 3 ]
then
  echo "[ERROR] not enough argument"
  exit
fi



# start ec2 instances and make sure that they are ready
./awsStart.sh $5
sleep 60

var=1
# docker magic
array=($(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" --query "Reservations[*].Instances[*].{IP:PublicIpAddress}" --output text))
for i in "${array[@]}"
do
  ssh -ttt -o StrictHostKeyChecking=no -i $KEY ec2-user@$i bash -c "'
    ./ec2client.sh $1 $2 $3 $4 $5 $var $6
  '" &
 ((var++)) 
done
wait

for i in "${array[@]}"
do
  ssh -ttt -i $KEY ec2-user@$i bash -c "'
    echo "run chown"
    sudo chown ec2-user:ec2-user tmp/*.gif
    echo "run chmod"
    sudo chmod 777 tmp/*.gif
    mv tmp/*.gif mnt/
  '"
done

ssh -ttt -i $KEY ec2-user@${array[0]} bash -c "'
  ./gm convert -loop 0 -delay 0  mnt/*.gif mnt/final.gif
'"


scp -i $KEY ec2-user@${array[0]}:mnt/final.gif ~/Desktop

ssh -ttt -i $KEY ec2-user@${array[0]} bash -c "'
  sudo rm -f mnt/*.gif
'"



var express = require('express');
var router = express.Router();
var sh = require('shelljs');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/execute', function(req, res, next){
      
  var AWSAccessKeyId=req.query.AWSAccessKeyId;
  var AWSSecretKey=req.query.AWSSecretKey;
  var bucketname=req.query.bucketname;
  var frames=req.query.frames;
  var instances=req.query.instances;
  var povfile=req.query.povfile;      						
  var executionstring = './main.sh '.concat(AWSAccessKeyId).concat(" ")
  	.concat(AWSSecretKey).concat(" ").concat(bucketname).concat(" ").concat(frames).concat(" ")
  	.concat(instances).concat(" ").concat(povfile);
  	console.log(executionstring);
  res.json(sh.exec(executionstring));
  //main script

  //exec docker run -v bla dockerimage script noFrame, noInstance, thisInstance
  //});
});

router.get('/terminate', function(req,res, next){
  //Poll a folder
  res.json(sh.exec('./awsStop.sh'));
});

module.exports = router;


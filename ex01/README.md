#Verteilte Systeme++


Blatt 01, 20.03.2015

Michael Tscholl (lab619)

In cooperation with Patrick Ober

1. done		1 point
2. done 	1 point
3. done 	1 point
4. done 	2 point
5. done 	1 point
6. done 	2 point
7. done 	1 point
8. done 	1 point

	total 10 point

##PARALLEL MOVIE RENDERING WITH POV-RAY

1. Choose one parallel machine and render the PNG images corresponding to the INI file on one slave processor using the locally available batch queuing system:
	
	`povray scherk.ini +Ischerk.pov +Oscherk.png +FN +W1024 +H768`

		look at `calcPpar.sh`

2. Merge all PNG files into one single gif file using the gm program;
	
	`gm convert -loop 0 -delay 0 *.png scherk.gif`

		look at `calcPpar.sh`
	
3. Measure the execution time of steps 1-2 and call it Tseq;
			
 		`echo "./calcPpar.sh LOOP" | qsub -cwd`
 		LOOP .... interation
	
		in the same dir the will be a file called time.txt with my results.
		My results after 10 trys are:
		- avg time[s]:  293
		- povary[s]:  262     
		- gm[s]: 31

	
4. Choose a large total number of frames in the INI file and write a small program that equally splits the frames based on the number of processors of the parallel machine. 

		`./paralle.sh M N`
		M .... frames
		N .... processors
	
	
5. Render the PNG files in parallel on at least 16 parallel processors using the job submission system. 

		1. run the `./clean.sh` to remove all old date
		2. run `./paralle.sh 20 16`
		3. run `./calcPpar.sh` and take the slowest time

6. Measure the new execution time and call it Tpar;

		after 10 repetition i got a avg time of 29,7s Tpar 
	 
7. Calculate the speedup of your application as follows: 

		S = Tseq / Tpar = 262s / 29,7s = 8.821548822
	
	
8. Calculate the efficiency of your application as follows: E = S/N, where S is the speedup and N is the number of processors.

		E = S / N = 8.821548822 / 16 = 0.551346801


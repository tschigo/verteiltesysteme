#!/bin/bash

MAX="10"
START="0"
TIME="0";

while [ $START -lt $MAX ]
do
  `./clear`
  start=`date +%s`
  povray scherk.ini +Ischerk.pov +Oscherk.png +FN +W1024 +H768
  povrayTime=`date +%s`
  ./gm convert -loop 0 -delay 0 *.png scherk.gif
  end=`date +%s`

  echo "Total[s]: " $((end-start)) "    povary[s]: " $((povrayTime-start)) "    gm[s]" $((end-povrayTime)) >> time.txt
  TIME=$((TIME+end-start))

  START=$[$START+1]
done
echo "avg time: " $((TIME/MAX)) >> time.txt


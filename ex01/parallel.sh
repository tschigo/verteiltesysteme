#!/bin/bash

if [ "$#" -ne 2 ]
then
  echo "processors/ frames missing!"
  exit 1
fi
M=$1
N=$2

echo "processor: " $N
echo "frames: " $M

fpp=$((M/N))
echo "fpp: "$fpp

START="1"

`rm -rf tmp`
`mkdir tmp`
help="1"

F_START="0"
F_END="0"

while [ $START -le $N ]
do
    F_START=$[F_END+1]
  if [ $((M%N)) -ge $START ]; then
    F_END=$[$F_END+$fpp+1]
  else
    F_END=$[$F_END+$fpp]
  fi

  echo "Antialias=Off" >> tmp/scherk_$START.ini
  echo "Antialias_Threshold=0.1" >> tmp/scherk_$START.ini
  echo "Antialias_Depth=2" >> tmp/scherk_$START.ini
  echo "Initial_Clock=0" >> tmp/scherk_$START.ini
  echo "Final_Clock=1" >> tmp/scherk_$START.ini
  echo "Cyclic_Animation=on" >> tmp/scherk_$START.ini
  echo "Pause_when_Done=off" >> tmp/scherk_$START.ini
  echo "Initial_Frame=1" >> tmp/scherk_$START.ini
  echo "Final_Frame="$M >> tmp/scherk_$START.ini

  echo "Subset_Start_Frame="$F_START >> tmp/scherk_$START.ini
  echo "Subset_End_Frame="$F_END >> tmp/scherk_$START.ini

  START=$[$START+1]
done

START="1"
while [ $START -le $N ]
do
  echo "povray tmp/scherk_"$START".ini +Ischerk.pov +Oscherk.png +FN +W1024 +H768" | qsub -cwd
  START=$[$START+1]
done



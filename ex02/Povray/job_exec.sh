#!/bin/bash

T_KARWENDEL_START=`date +%s`

# del all old files
rm *.png *.gif
rm job_povray.sh.o* job_gm.sh.o*
rm -rf tmp/ time.csv

if [ "$#" -ne 2 ]
then
  echo "processors/ frames missing!"
  exit 1
fi

# create ini files
M=$1
N=$2


fpp=$((M/N))

START="1"

`rm -rf tmp`
`mkdir tmp`
help="1"

F_START="0"
F_END="0"

echo "Creating ini files"
while [ $START -le $N ]
do
    F_START=$[F_END+1]
  if [ $((M%N)) -ge $START ]; then
    F_END=$[$F_END+$fpp+1]
  else
    F_END=$[$F_END+$fpp]
  fi

  echo "Antialias=Off" >> tmp/scherk_$START.ini
  echo "Antialias_Threshold=0.1" >> tmp/scherk_$START.ini
  echo "Antialias_Depth=2" >> tmp/scherk_$START.ini
  echo "Initial_Clock=0" >> tmp/scherk_$START.ini
  echo "Final_Clock=1" >> tmp/scherk_$START.ini
  echo "Cyclic_Animation=on" >> tmp/scherk_$START.ini
  echo "Pause_when_Done=off" >> tmp/scherk_$START.ini
  echo "Initial_Frame=1" >> tmp/scherk_$START.ini
  echo "Final_Frame="$M >> tmp/scherk_$START.ini

  echo "Subset_Start_Frame="$F_START >> tmp/scherk_$START.ini
  echo "Subset_End_Frame="$F_END >> tmp/scherk_$START.ini

  START=$[$START+1]
done
echo "done."

# start all jobs
T_KARWENDEL_END=`date +%s`
echo "karwendel.dps.uibk.ac.at, $T_KARWENDEL_START, $T_KARWENDEL_END, createscripts" >> time.csv

START="1"
while [ $START -le $N ]
do
  qsub job_povray.sh $START
  START=$[$START+1]
done
T_JOBSC_END=`date +%s`
echo "karwendel.dps.uibk.ac.at, $T_KARWENDEL_END, $T_JOBSC_END, submitjobs" >> time.csv



# wait for all jobs
while [ "`qstat`" != "" ]
do
  sleep 5
done

echo "All jobs are finished"

echo "Creating gif ..."
# create gif
qsub job_gm.sh
echo "done."




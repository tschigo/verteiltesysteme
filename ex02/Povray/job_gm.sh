#!/bin/bash
#$ -cwd
#$ -j y
#$ -sync y

START=`date +%s`
./gm convert -loop 0 -delay 0 *.png scherk.gif
END=`date +%s`

echo "$HOSTNAME, $START, $END, gm executing $JOB_ID" >> time.csv


#!/bin/bash
#$ -cwd
#$ -j y

START=`date +%s`
povray tmp/scherk_$1.ini +Ischerk.pov +Oscherk.png +FN +W1024 +H768
END=`date +%s`


echo "$HOSTNAME, $START, $END, povray executing $JOB_ID" >> time.csv


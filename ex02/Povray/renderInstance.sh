# render on instance

START=`date +%s`
povray tmp/scherk_$1.ini +Ischerk.pov +Oscherk.png +FN +W1024 +H768
END=`date +%s`


echo "instance$1, $START, $END, povray executing" >> time$1.csv

rsync -az $POVRAY "$2:~/all"

EEND=`date +%s`
echo "instance$1, $START, $EEND, uploading files" >> time$1.csv
rsync -az time$1.csv "$2:~/times/"
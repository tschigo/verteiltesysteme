#Verteilte Systeme++


Sheet 02, 27.03.2015

Michael Tscholl (lab619)

In cooperation with Lukas Huber and Patrick Ober


1. done		2 point
2. done 	3 point
3. done 	1 point
4. done 	1 point
5. done 	1 point
6. done 	2 point

	total 10 point
	
##MOVIE RENDERING APPLICATION DEPLOYMENT


You might use the results from the previews exercise sheet to solve the tasks requested here. The goal is to write an application that is able to copy input files and binaries onto a cluster (karwendel), then render the movie scene (in parallel if done for the last sheet) and then fetches the resulting .gif and cleaning up the cluster afterwards. The cleanup job should be optional, to allow a presentation without cleanup in the exercise! Possible usage:

`[myPC] $ ./myRemoteRenderer.sh lab600@karwendel.dps.uibk.ac.at 16 cleanup`

1. Write an application that is able to copy / stage-in all the needed files for rendering the movie on a cluster.
		
		./myRemoteRenderer.sh USERNAME PROCESSOR [cleanup]
		USERNAME  ... username on krawendel
		PROCESSOR ... number of qsubs
		cleanup   ... optional, if set all remote files will be deleted
		
		the results are a ganttchart(time.jpg) and a csv-file(Povray/time.csv) with all the data.
		
		NOTE: install reshape2 and ggplot2 for R, for details look at point 6. otherwise the will be no ganttchart

2. Extend the application to execute the rendering using the job submission system on the cluster.

		look at job_exec.sh
		
3. The execution times of each render job and their execution host has to be collected including their start or end time.

		look at job_povray.sh

4. After the rendering the merge job should be executed followed by a collection job, that transfers the resulting animation to the origin PC.

		look at job_gm.sh
		
5. Then if selected the application should clean up the cluster drive from the files put there. Make sure only generated files are deleted and no other files! You cannot assume an empty home directory!

		just add cleanup as last parameter and all remote files will be deleted before disconecting.
		
6. Draw a gantt chart of the execution using the collected timings of the render and merger job and their mapping to resources.

		Start R and install 2 packages:
		->R
		>install.packages("reshape2")
		>install.packages("ggplot2")

		the rest is done by the myRemoteRenderer.sh script, as output you will get a ganttchart(time.jpg).
		
		
<br >
<br >
<br >
<br >
<br >
<br >
<br >
<br >
<br >
<br >
<br >

#####16 Processors

![image](pics/tscholl_time16_2.jpg =320x "time p16")

#####40 Processors
![image](pics/tscholl_time40_2.jpg =300x "time p40")
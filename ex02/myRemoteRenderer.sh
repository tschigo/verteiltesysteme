#!/bin/bash
T_START=`date +%s`
################################################################################
# ~>./myRemoteRenderer.sh lab600 16 cleanup                                    #
################################################################################

if [ "$#" -lt 2 ]
then
  echo "parameter missing!"
  exit 1
fi

SERVER="$1@karwendel.dps.uibk.ac.at"
DEST="~/$1_Povray_tmp"
POVRAY="Povray"

#create csv
echo "jobs,start.time,end.time,jobdetails" > Povray/time.csv

# Upload
T_UPLOAD_START=`date +%s`
echo "Uploading ..."
rsync -az $POVRAY "$SERVER:$DEST"
T_UPLOAD_END=`date +%s`
echo "client , $T_UPLOAD_START, $T_UPLOAD_END, uploading" >> Povray/time.csv
echo "done."

# render images and create gif
T_SSH_START=`date +%s`
ssh $SERVER bash -c "'
  cd $DEST/Povray;
  chmod +x job_exec.sh;
  ./job_exec.sh 100 $2;
'"
  T_SSH_END=`date +%s`
  echo "client , $T_SSH_START, $T_SSH_END, jobexecution" >> Povray/time.csv

# download gif
echo "Downloading gif and results ..."
T_DOWNLOAD_START=`date +%s`
rsync -t "$SERVER:$DEST/Povray/scherk.gif" scherk.gif
T_DOWNLOAD_END=`date +%s`
echo "client , $T_DOWNLOAD_START, $T_DOWNLOAD_END, download File" >> Povray/time.csv

rsync -t "$SERVER:$DEST/Povray/time.csv" Povray/remotetime.csv
T_DOWNLOAD_CSV=`date +%s`
echo "client , $T_DOWNLOAD_END, $T_DOWNLOAD_CSV, download Time" >> Povray/time.csv
echo "done."

# cleanup
T_CLEANUP_START=`date +%s`
if [ "$3" == "cleanup" ]
then
  echo "cleanup ..."
  ssh $SERVER bash -c "'
    rm -rf $DEST
  '"
  echo "done."
fi
T_CLEANUP_END=`date +%s`
echo "client , $T_CLEANUP_START, $T_CLEANUP_END, cleanup" >> Povray/time.csv

T_END=`date +%s`
echo "client , $T_START, $T_END, overall" >> Povray/time.csv

cat Povray/remotetime.csv >> Povray/time.csv
echo "Generating ganttchart ..."
R CMD BATCH createGanttchart.R
echo "done."

exit 0

#!/bin/bash

WORK="/home/ec2-user/Povray/"
IMG="/home/ec2-user/mnt/img_povray/"

if [ "$#" -lt 1 ]
then
  echo "Set images"
  exit 1;
fi


# wait until all images are processed
cd $WORK;
while true; do
  num=`ls $IMG | wc -l`
  if [[ $num -lt $1 ]]; then
    sleep 2;
  else
    break
  fi
done

# render gif
./gm convert -loop 0 -delay 0 $IMG*.png scherk.gif

# remove old images
rm -rf $IMG*.png
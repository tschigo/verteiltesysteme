#!/bin/bash

if [ "$#" -lt 1 ]
then
  echo "S3 bucket not set!"
  exit 1;
fi

WORK="/home/ec2-user/Povray/"

# create images
cd $WORK
./povray scherk.ini +Ischerk.pov +Oscherk.png +FN +W1024 +H768

# mount s3 volume and move into bucket
s3fs $1 ~/mnt/
mount
mv *.png ~/mnt/img_povray/

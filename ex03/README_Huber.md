#IAAS CLOUD COMPUTING (15+3 POINTS)
The basic steps required to use EC2 command line tools will be shown in the Lab. For this sheet you will need to render our movie on two instances of EC2 (preferable develop on the free tier) and then run it on 3 different instance types to get some speedup: 

1. Set up your environment to use your EC2 keys to communicate with the Cloud. Set port access on your instances to allow the services you need; (1 points) 
 
   Used the AWS Website to create a Security Group UIBK which is only allowed to access from the VPN inside the University Network.

2. Start an EC2 instance of your choice and prepare it for the execution of our povray workflow. All applications need to be executed on this resource: povray and gm;
(2 points)

    ```
    aws ec2 run-instances --image-id  ami-df6a8b9b --count 1 --instance-type t2.micro --key-name my-first-key --security-groups UIBK
    ```
    I created an AMI Image by running an instance from the terminal, configuring it via ssh and then use the AWS tools to create an Image out of it.
    
    ```
    sudo apt-get install build-essential git libfuse-dev libcurl4-openssl-dev libxml2-dev mime-support automake libtool
    sudo apt-get install pkg-config libssl-dev # See (*3)
    git clone https://github.com/s3fs-fuse/s3fs-fuse
    cd s3fs-fuse/
    ./autogen.sh
    ./configure --prefix=/usr --with-openssl # See (*1)
    make
    sudo make install
    ```

3. Write a application that can be executed locally, that starts a given number of Cloud instance and automatically sets up the execution environment on the resource (or uses images with preinstalled applications. Make sure the image is public and usable by Simon!); (4 points)

4. Extend the previews cluster script to render the given movie on your clouds instances. The usage of your script should be similar to this (supporting more then 2 instances/ip addresses!):
    ```
    cloudRender.sh 192.168.0.1 192.168.0.2 …
    ```
You can use any technology possible. The easiest solution might be a combination of sftp and ssh. More elegant could be the installation of a web service on the Cloud (3 bonus points!)  (4+3 points)
5. Measure the execution time of the Cloud rendering and the time it takes to start and stop the  Cloud instances (this can be 3 different independent scripts) for 3 different instance types. 
 
Calculate the speedup and efficiency of those compared with the free tier type you used for the development of the scripts. 
## launch a aws ec2 amazon linux instance


look at http://docs.aws.amazon.com/cli/latest/userguide/cli-using-ec2.html

Create a Key Pair for ec2
`aws ec2 create-key-pair --key-name bisaknosp-us-west-1 --query 'KeyMaterial' --output text > bisaknosp-us-west-1.pem`

set permission
`chmod 400 Bisaknosp`

Create a new security-group(NidoranSecurityGroup)
`aws ec2 create-security-group --group-name NidoranSecurityGroup --description "Nidoran security group"`

    {
        "GroupId": "sg-c55ce9a0"
    }

Allow access on port 22 and 3389 for ssh and rdp
add RDP
`aws ec2 authorize-security-group-ingress --group-id sg-c55ce9a0 --protocol tcp --port 3389 --cidr 0.0.0.0/0`

add SSH 
`aws ec2 authorize-security-group-ingress --group-id sg-c55ce9a0 --protocol tcp --port 22 --cidr 0.0.0.0/0`


look at all rules if they are correct `aws ec2 describe-security-groups`


start a new instance
`aws ec2 run-instances --image-id  ami-d114f295 --count 1 --instance-type t2.micro --key-name bisaknosp-us-west-1 --security-groups NidoranSecurityGroup
`


connect to server
`ssh -i /path/key_pair.pem ec2-user@public_dns_name`



2.

`aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" --query "Reservations[0].Instances[*].{ID:InstanceId, DNS:PublicDnsName, IP:PublicIpAddress}"`

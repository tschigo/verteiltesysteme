#!/bin/bash

INSTANCES=1
KEY_NAME="lab613-uswest1"
SECURITY_GROUP="UIBK"
AWS_TYPE="ami-8f2ccecb"
INSTANCE_TYPE="m3.large"

if [ "$#" -gt 0 ]
then
  INSTANCES=$1
fi


T_START=`date +%s`

aws ec2 run-instances --image-id  $AWS_TYPE --count $INSTANCES --instance-type $INSTANCE_TYPE --key-name $KEY_NAME --security-groups $SECURITY_GROUP


count=0;
while ((count < $INSTANCES)); do
  count=($(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" --query "Reservations[*].Instances[*].{IP:PublicIpAddress}" --output text | wc -l))
  echo $count "instances found!";
  sleep 2;
done

T_END=`date +%s`

TIME=$((T_END-T_START-2))

echo "$T_START, $T_END, $TIME, start_instances" >> time.csv;

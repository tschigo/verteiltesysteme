#!/bin/bash

T_AWS_START=`date +%s`
array=($(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" --query "Reservations[0].Instances[*].{ID:InstanceId}" --output text))
for i in "${array[@]}"
do
  aws ec2 terminate-instances --instance-ids $i
done
T_AWS_END=`date +%s`

echo "$T_AWS_START, $T_AWS_END,$((T_AWS_END-T_AWS_START)) End_aws" >> time.csv

# Lukas Huber
# Patrick Ober
# Michael Tscholl
# Cloud Renderer on Amazon Web Services

################################################################################
# cloudRenderer.sh 192.168.0.1 192.168.0.2
# Distributes and runs the jobs on the AWS instances
################################################################################

# Take input ip's

IPS=("$@")

echo "ips are ${IPS[@]}"
# create ini files
# Number of Frames
M=4
# Number of instances
N=$#

fpp=$((M/N))

START="1"

`rm -rf tmp`
`mkdir tmp`


F_START="0"
F_END="0"

echo "Creating ini files"
while [ $START -le $N ]
do
    F_START=$[F_END+1]
  if [ $((M%N)) -ge $START ]; then
    F_END=$[$F_END+$fpp+1]
  else
    F_END=$[$F_END+$fpp]
  fi

  echo "Antialias=Off" >> tmp/scherk_$START.ini
  echo "Antialias_Threshold=0.1" >> tmp/scherk_$START.ini
  echo "Antialias_Depth=2" >> tmp/scherk_$START.ini
  echo "Initial_Clock=0" >> tmp/scherk_$START.ini
  echo "Final_Clock=1" >> tmp/scherk_$START.ini
  echo "Cyclic_Animation=on" >> tmp/scherk_$START.ini
  echo "Pause_when_Done=off" >> tmp/scherk_$START.ini
  echo "Initial_Frame=1" >> tmp/scherk_$START.ini
  echo "Final_Frame="$M >> tmp/scherk_$START.ini

  echo "Subset_Start_Frame="$F_START >> tmp/scherk_$START.ini
  echo "Subset_End_Frame="$F_END >> tmp/scherk_$START.ini

  START=$[$START+1]
done
echo "done."
# Copy files to instances

DEST="~/Povray"
POVRAY="Povray/"
COUNT=1
for i in "${IPS[@]}"
do
  # Upload
  T_UPLOAD_START=`date +%s`
  echo "Uploading ... no $COUNT"
  # TODO upload security key
  #ssh -i my-first-key.pem  -o StrictHostKeyChecking=no ubuntu@$i  "mkdir ~/Povray/" 
  rsync -rave "ssh -i my-first-key.pem -o StrictHostKeyChecking=no" Povray/ tmp/scherk_$COUNT.ini renderInstance.sh passwd-s3fs "ubuntu@$i:Povray/"
  COUNT=$((COUNT + 1))
  T_UPLOAD_END=`date +%s`
echo "$i , $T_UPLOAD_START, $T_UPLOAD_END, uploading" >> time.csv
echo "done."
done


COUNT=1
#MAIN="$IPS[1]"
BUCKET="povbucket"
# render images and create gif
T_SSH_START=`date +%s`
for i in "${IPS[@]}"
do
ssh -i my-first-key.pem  -o StrictHostKeyChecking=no ubuntu@$i bash -c "'
  cd ~/Povray
  ./renderInstance.sh  $COUNT $BUCKET
'" 
#ssh -i my-first-key.pem ubuntu@$i "screen -d -m ./Povray/renderInstance.sh  $COUNT $BUCKET"
COUNT=$((COUNT + 1))
done
echo "Started Rendering on Instances $COUNT"
echo "$i , $T_SSH_START, $END, start execution" >> time.csv
END=`date +%s`
exit 1;
#do render commands
  T_SSH_END=`date +%s`

sleep 10

# merge to gif using gm
for i in "${IPS[@]}"
do
	echo "starting gm script and waiting"
rsync -az -e "ssh -i my-first-key.pem  -o StrictHostKeyChecking=no" gmRender.sh "ubuntu@$i:Povray/"
ssh -i my-first-key.pem  -o StrictHostKeyChecking=no ubuntu@$i bash -c "'
  cd ~/Povray
  chmod +x gmRender.sh
  ./gmRender.sh $BUCKET $M
'"
T_JOBSC_END=`date +%s`
#echo "client, $T_KARWENDEL_END, $T_JOBSC_END, submitjobs" >> time.csv

# copy files back to executing computer


# get Time from Instances
rsync -az -e "ssh -i my-first-key.pem  -o StrictHostKeyChecking=no" "ubuntu@$i:~/Povray/scherk.gif" all/scherk.gif
rsync -az -e "ssh -i my-first-key.pem  -o StrictHostKeyChecking=no" "ubuntu@$i:~/Povray/time.csv" all/time.csv
T_DOWNLOAD_CSV=`date +%s`
echo "client , $T_DOWNLOAD_END, $T_DOWNLOAD_CSV, download Time" >> time.csv
echo "done."

cat all/time.csv >> time.csv
R CMD BATCH createGanttChart.R
break
done
done

#!/bin/bash

if [ "$#" -lt 2 ]
then
  echo "Set frames and instances"
  exit 1;
fi


#frames
M=$1

#instances
N=$2


# calc ini files
fpp=$((M/N))

START="1"

`rm -rf tmp`
`mkdir tmp`
help="1"

F_START="0"
F_END="0"

# create ini files
echo "Creating ini files"
while [ $START -le $N ]
do
    F_START=$[F_END+1]
  if [ $((M%N)) -ge $START ]; then
    F_END=$[$F_END+$fpp+1]
  else
    F_END=$[$F_END+$fpp]
  fi

  echo "Antialias=Off" >> tmp/scherk_$START.ini
  echo "Antialias_Threshold=0.1" >> tmp/scherk_$START.ini
  echo "Antialias_Depth=2" >> tmp/scherk_$START.ini
  echo "Initial_Clock=0" >> tmp/scherk_$START.ini
  echo "Final_Clock=1" >> tmp/scherk_$START.ini
  echo "Cyclic_Animation=on" >> tmp/scherk_$START.ini
  echo "Pause_when_Done=off" >> tmp/scherk_$START.ini
  echo "Initial_Frame=1" >> tmp/scherk_$START.ini
  echo "Final_Frame="$M >> tmp/scherk_$START.ini

  echo "Subset_Start_Frame="$F_START >> tmp/scherk_$START.ini
  echo "Subset_End_Frame="$F_END >> tmp/scherk_$START.ini

  START=$[$START+1]
done
echo "done."

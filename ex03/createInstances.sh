#!/bin/sh
# Lukas Huber
# Patrick Ober
# Michael Tscholl
################################################################################
# ~>./createInstances.sh 16                                                    #
################################################################################
T_START=`date +%s`
# Input
rm output.json

if [ "$#" -lt 1 ]
then
  echo "parameter missing!"
  exit 1
fi

INSTANCES="$1"
#INSTANCE="$2"
#POVRAY="Povray"

for i in {1..$1}
# Start Instance
do
 aws ec2 run-instances --image-id  ami-3721c373 --count 1 --instance-type t2.micro --key-name my-first-key --security-group-ids sg-365ee853 --associate-public-ip-address  >> output.json
 echo "instance $1 created"
done
# Get Information IP, instance-id, ...
IPS="$(cat output.json | jq '.Instances[].PublicIpAddress')"

IDS="$(cat output.json | jq '.Instances[].InstanceId')"

echo "Public IPs: $IPS"
echo "$Instance IDs: $IDS"

# ssh connect to instance
#ssh -i my-first-key.pem ubuntu@52.8.13.41
#sudo locale-gen de_AT.UTF-8
 # Load GM and Povray to it




 #aws ec2 terminate-instances --instance-ids i-03ba42c8 #$IDS
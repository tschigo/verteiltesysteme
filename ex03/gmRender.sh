#!/bin/sh

# gmRender.sh BUCKET FRAMES

IMG="~/mnt/img_povray1/"

#cat passwd-s3fs > ~/.passwd-s3fs
#chmod 400 ~/.passwd-s3fs 

if [ ! -d ~/mnt/img_povray1/ ]; then
s3fs povbucket ~/mnt/
mount
fi

T_GM_START=`date +%s`
# Wait for all instances
FRAMES=1
while [ "$2" != "$FRAMES" ] 
do
FRAMES=$(ls ~/mnt/img_povray1/ | wc -l)
echo "waiting gm"
sleep 2
done

T_GM_WAIT=`date +%s`
echo "Main, $T_GM_START, $T_GM_WAIT, wait for instances" >> time.csv

# render gif
./../gm convert -loop 0 -delay 0 $IMG*.png scherk.gif

rm -rf $IMG*.png
mv scherk.gif $IMGscherk.gif
T_GM_END=`date +%s`
echo "Main, $T_GM_WAIT, $T_GM_END, merging gif" >> time.csv

cat ~/mnt/time1/*.csv >> time.csv
# remove old images
rm -rf $IMG*.png

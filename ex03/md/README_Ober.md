#Verteilte Systeme++

Patrick Ober

In cooperation with Michael Tscholl and Lukas Huber

Exercises solved:

1. ✓ (1 point)
2. ✓ (2 points)
3. ✓ (4 points)
4. ✓ (4 points)
5. ✓ (4 points)

total: 15 points.

###IAAS CLOUD COMPUTING

The basic steps required to use EC2 command line tools will be shown in the Lab. For this sheet you will need to render our movie on two instances of EC2 (preferable develop on the free tier) and then run it on 3 different instance types to get some speedup

1. **Set up your environment to use your EC2 keys to communicate with the Cloud. Set port access on your instances to allow the services you need**
		Create a Key Pair for ec2 and set permission
		`aws ec2 create-key-pair --key-name lab613-uswest1 --query 'KeyMaterial' --output text > lab613-uswest1.pem`
		`chmod 400 lab613-uswest1.pem`
		
		Create a new security-group(NidoranSecurityGroup)
		`aws ec2 create-security-group --group-name UIBK --description "Security group for UIBK"`
		
		Get information about the created security-group (e.g. group-id) 
		`aws ec2 describe-security-groups`
		
		Allow access on port 22 for ssh/scp/rsync
		`aws ec2 authorize-security-group-ingress --group-id <group-id> --protocol tcp --port 22 --cidr 0.0.0.0/0`
				
		Start an instance
		`aws ec2 run-instances --image-id  $ID --count $COUNT --instance-type $INSTANCE --key-name lab613-uswest1 --security-groups UIBK
		
		Connect to instance
		`ssh -i /path/key_pair.pem ec2-user@public_dns_name`
		or
		`ssh -i /path/key_pair.pem ec2-user@public_ip`
		
		Everything can also be done with the EC2 web interface.


2. **Start an EC2 instance of your choice and prepare it for the execution of our povray workflow. All applications need to be executed on this resource: povray and gm**
	
	I started an instance of the aws-linux public image and prepared it to contain povray and gm. Also two scripts were included to execute the rendering jobs. Furthermore I installed s3fs to mount a S3 volume, where all rendered images over several instances are stored.
	To start an instance, look at `awsStart.sh`.
	Change the following parameters in `awsStart.sh` to your account setting:
	-KEY_NAME
	-SECURITY_GROUP	
	
	*Note: Guide to install s3fs `https://github.com/s3fs-fuse/s3fs-fuse/wiki/Installation-Notes`*
	
3. **Write a application that can be executed locally, that starts a given number of Cloud instance and automatically sets up the execution environment on the resource**
	
	Call `awsStart.sh` with the number of instances you want to be created. The script ensures, that the instances are running when it finishes.
	Use `awsShow.sh` to get information about running instances (e.g. the IPs).

4. **Extend the previews cluster script to render the given movie on your clouds instances. The usage of your script should be similar to this**
	
	Before executing the rendering script:
		1. Create a S3 bucket
			- Go to console.aws.amazon.com/s3/home
			- Create a new bucket
			- Create an access key(Access Key ID and Secret Access Key). Click on your username in the right upper corner and go into Security Credentials. Here, create an access key and download the .csv file.
	
		2. Modify `cloudRender.sh` to fit your settings:
			- KEY="/path/to/key.pem"
			- AWSAccessKeyId="Access Key Id from .csv"
			- AWSSecretKey="Secret Access Key from .csv"
			- S3BUCKET="yourBucketName"
			
						
	To call the rendering jobs remotely, use `cloudRender.sh` and list the IPs of instances that you want to use.
	`cloudRender.sh` creates the .ini files distributed onto the instances and calls `exec_jobs.sh` and `exec_gm.sh` remotely. `exec_jobs.sh` executes the povray program and mounts the given S3 bucket. After the rendering is done, the instances move the created .png files onto the bucket. `exec_gm.sh` is running simultaneously and polls the bucket until all images are there. Afterwards, the gm program is executed and the created .gif is moved onto the local machine. 


	To terminate all instances, use `awsStop.sh`

5. **Measure the execution time of the Cloud rendering and the time it takes to start and stop the Cloud instances (this can be 3 different independent scripts) for 3 different instance types. Calculate the speedup and efficiency of those compared with the free tier type you used for the development of the scripts.**
	
	The time is measured during the execution of the scripts and stored into time.csv.
	
	Measurements were made with 3 different instance types: t2.micro, t2.medium, m3.large.
	I used 20 frames for rendering and 4 instances for the parallel measurement.
	
	Specifications:
	
	
	Speedup.....S
	Efficiency..E
	
	
	t2.micro:
		Tseq = 225s
		Tpar = 88s
		
		S = Tseq / Tpar = 2.55
		E = S / N = 0.64

	t2.medium:
		Tseq = 221s
		Tpar = 88s
		
		S = Tseq / Tpar = 2.51
		E = S / N = 0.63
		
	m3.large:
		Tseq = 220s
		Tpar = 90s
		
		S = Tseq / Tpar = 2.44
		E = S / N = 0.61

	Measurements:
		20 frames, 1 instance
		t2.micro_seq:
		1429167483, 1429167510, 25, start_instances
		1429167894, 1429167897, 3, create_and_upload_ini
		1429167897, 1429168114, 217, gm
		1429168114, 1429168119, 5, download_gif
		1429167894, 1429168119, 225, remote_time
		1429168214, 1429168218, 4, end_aws

		t2.medium_seq:
		1429168286, 1429168317, 29, start_instances
		1429168521, 1429168524, 3, create_and_upload_ini
		1429168524, 1429168739, 215, gm
		1429168739, 1429168742, 3, download_gif
		1429168521, 1429168742, 221, remote_time
		1429168854, 1429168859, 5, end_aws

		m3.large_seq:
		1429168930, 1429168953, 21, start_instances
		1429169387, 1429169390, 3, create_and_upload_ini
		1429169390, 1429169604, 214, gm
		1429169604, 1429169607, 3, download_gif
		1429169387, 1429169607, 220, remote_time
		1429169654, 1429169659, 5, end_aws

		20 frames, 4 instances
		t2.micro:
		1429100184, 1429100212, 27, start_instances
		1429099827, 1429099839, 12, create_and_upload_ini
		1429099839, 1429099911, 72, gm
		1429099911, 1429099915, 4, download_gif
		1429099827, 1429099915, 88, remote_time
		1429100107, 1429100119, 12, end_aws

		t2.medium:
		1429100184, 1429100212, 26, start_instances
		1429100423, 1429100437, 14, create_and_upload_ini
		1429100437, 1429100508, 71, gm
		1429100508, 1429100511, 3, download_gif
		1429100423, 1429100511, 88, remote_time
		1429100554, 1429100566, 12, end_aws

		m3.large:
		1429100672, 1429100704, 30, start_instances
		1429100786, 1429100801, 15, create_and_upload_ini
		1429100801, 1429100873, 72, gm
		1429100873, 1429100876, 3, download_gif
		1429100786, 1429100876, 90, remote_time
		1429100887, 1429100898, 11, end_aws
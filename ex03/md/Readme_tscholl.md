#Verteilte Systeme++


Sheet 02, 16.04.2015

Michael Tscholl (lab619)

In cooperation with Lukas Huber and Patrick Ober
files: awsStart.sh, cloudRender.sh, showInstances.sh, exec_gm.sh, exec_job.sh

1. done		1 point
2. done 	2 point
3. done 	4 point
4. done  	4 + 3 point
5. done 	4 point

	total 15 + 3 point
	
###IAAS CLOUD COMPUTING

The basic steps required to use EC2 command line tools will be shown in the Lab. For this sheet you will need to render our movie on two instances of EC2 (preferable develop on the free tier) and then run it on 3 different instance types to get some speedup

1. Set up your environment to use your EC2 keys to communicate with the Cloud. Set port access on your instances to allow the services you need
	
		Create a Key Pair for ec2 and set permission
		`aws ec2 create-key-pair --key-name bisaknosp-us-west-1 --query 'KeyMaterial' --output text > bisaknosp-us-west-1.pem`
		`chmod 400 bisaknosp-us-west-1.pem`
		
		Create a new security-group(NidoranSecurityGroup)
		`aws ec2 create-security-group --group-name NidoranSecurityGroup --description "Nidoran security group"`
		
		Allow access on port 22 for ssh/scp/rsync
		`aws ec2 authorize-security-group-ingress --group-id sg-c55ce9a0 --protocol tcp --port 3389 --cidr 0.0.0.0/0`
		
		Look at all rules if they are correct 
		`aws ec2 describe-security-groups`
		
		Start a new instance
		`aws ec2 run-instances --image-id  $ID --count $COUNT --instance-type $INSTANCE --key-name bisaknosp-us-west-1 --security-groups NidoranSecurityGroup
		
		Connect to server
		`ssh -i /path/key_pair.pem ec2-user@public_dns_name`
		`ssh -i /path/key_pair.pem ec2-user@public_ip`



		
2. Start an EC2 instance of your choice and prepare it for the execution of our povray workflow. All applications need to be executed on this resource: povray and gm
		
		look at awsStart.sh
		There is a custom instance image with povray, gm and s3fs to mount a s3 bucket.

3. Write a application that can be executed locally, that starts a given number of Cloud instance and automatically sets up the execution environment on the resource 


		awsStart.sh [Instances]
		Instances ... number of instances which will be created, if not set only one instance will be created

4. Extend the previews cluster script to render the given movie on your clouds instances. The usage of your script should be similar to this

	 `cloudRender.sh 192.168.0.1 192.168.0.2 ...`
	
	
		cloudRender.sh IP [IP ...]
		IP ... ip-adress of instance
		
		1. Create a S3 bucket
			a. Go to console.aws.amazon.com/s3/home
			b. Create a new bucket exp. mybucket
			c. Create a access key(Access Key ID and Secret Access Key). Click on your username in the right upper corner and click in the menu on Security Credentials. Continue to Security Credentials, create a access key and download .csv file
			
		2. Open cloudRender.sh in a editor
			line 9: set Access Key ID form .csv-file
			line 10: set Secret Access Key form .csv-file
			line 11: set the path to your key pair
			line 12: set the name of S3 bucket
			
			line 15: change default value for more frames
		
		3. showInstances.sh shows you the ip of all running instances which you can add when you are running the cloudRender.sh script
		
		
5. Measure the execution time of the Cloud rendering and the time it takes to start and stop the Cloud instances (this can be 3 different independent scripts) for 3 different instance types. Calculate the speedup and efficiency of those compared with the free tier type you used for the development of the scripts.



Useful 

 - aws docu  [link](http://docs.aws.amazon.com/cli/latest/userguide/cli-using-ec2.html)
 - s3fs wiki [link](https://github.com/s3fs-fuse/s3fs-fuse/wiki)



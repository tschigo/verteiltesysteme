#!/bin/bash
################################################################################
# ~>./renderCloud.sh            
# Orchestrates the execution overall                                           #
# Create Instances
# Run cloudRenderer.sh to render images and convert gif
################################################################################

T_START=`date +%s`

POVRAY="Povray"

# create Instances
./createInstances.sh 5

T_CREATEINSTANCE=`date +%s`

#create csv
echo "pc,$T_START,$T_CREATEINSTANCE,instance creation" > time.csv

IDS= cat output.json | jq '.Instances[].InstanceId'

while[ ${#IPS[@]} -lt 5 ]
do
IPS= "$(aws ec2 describe-instances --instance-ids $IDS | jq '.Reservations[].Instances[].PublicIpAddress')"
done
T_IPSETUP=`date +%s`
echo "pc,$T_CREATEINSTANCE,$T_IPSETUP,instance creation" >> time.csv

# Render on the Instances

./cloudRenderer.sh IPS





# cleanup
T_CLEANUP_START=`date +%s`
if [ "$3" == "cleanup" ]
then
  for i in "${IDS[@]}"
  do
  echo "cleanup ..."
  ssh $SERVER bash -c "'
    rm -rf $DEST
  '"
  echo "done."
  aws ec2 terminate-instances --instance-ids $IDS
  done
fi
T_CLEANUP_END=`date +%s`
echo "client , $T_CLEANUP_START, $T_CLEANUP_END, cleanup" >> time.csv

T_END=`date +%s`
echo "client , $T_START, $T_END, overall" >> time.csv

cat Povray/remotetime.csv >> time.csv
echo "Generating ganttchart ..."
R CMD BATCH createGanttchart.R
echo "done."

exit 0

#!/bin/sh

# renderInstance.sh NUMBER BUCKETNAME


#if [ ! -f "~/mnt/" ]; then
  	mkdir ~/mnt/
#fi

#if [ ! -d ~/mnt/img_povray1 ]; then
	# Sync with S3 bucket
    cat passwd-s3fs > ~/.passwd-s3fs
    chmod 400 ~/.passwd-s3fs 
  	s3fs $2 ~/mnt/
	mount
  	#mkdir ~/mnt/img_povray
#fi

if [ ! -d ~/mnt/time1 ]; then
	mkdir ~/mnt/time1
fi


START=`date +%s`
./../povray  scherk_$1.ini +Ischerk.pov +Oscherk.png +FN +W1024 +H768 
END=`date +%s`


echo "instance$1, $START, $END, povray executing" >> time$1.csv




mv *.png ~/mnt/img_povray1/


# copy to s3

EEND=`date +%s`
echo "instance$1, $START, $EEND, copying files to s3" >> time$1.csv
mv time$1.csv ~/mnt/time1/time$1.csv

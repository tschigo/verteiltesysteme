#!/bin/bash


array=($(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" --query "Reservations[*].Instances[*].{ID:InstanceId, DNS:PublicDnsName, IP:PublicIpAddress}"}" --output text))

for i in "${array[@]}"
do
  echo $i
done

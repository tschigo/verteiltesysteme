#!/bin/bash

KEY="/Users/Tschigo/.ssh/bisaknosp-us-west-1.pem"
POVRAY="Povray"

#upload gm and povrey
array=($(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" --query "Reservations[0].Instances[*].{IP:PublicIpAddress}" --output text))
for i in "${array[@]}"
do
  rsync -rave "ssh -i $KEY" $POVRAY ec2-user@$i:/home/ec2-user/
done
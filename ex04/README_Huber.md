#Verteilte Systeme++


Sheet 04, 24.04.2015

Lukas Huber (lab604, due to certificate problems, thanks to Michael Tscholl used lab619 )

In cooperation with Michael Tscholl and Patrick Ober
files: myRemoteRenderer.sh, upload.sh, showInstances.sh, exec_gm.sh, exec_job.sh, createIni.sh, createMetaIni.sh

Solved all exercises

1. 	1 point
2.  4 point
3.  5 point

	 total 10 points


##GRID COMPUTING (10 POINTS)


The basic steps required to use the Austrian Grid will be shown in the Lab. For this sheet you will need to render our movie on two sites of the following 3 Austrian Grid sites:

	 - karwendel.dps.uibk.ac.at
	 - login.leo1.uibk.ac.at
	 - lise.jku.austriangrid.at

1. Copy all applications need to be executed on the resource: povray and gm;
	
		
		./upload.sh HOST
		
		HOST ... can be karwendel.dps.uibk.ac.at, login.leo1.uibk.ac.at or lise.jku.austriangrid.at
		
		The script uploads all files needed for povray and gm and makes povray and gm executable

2. Write a application that can be executed on karwendel, that starts the povray movie rendering on the Grid resources and collects the result.

		./myRemoteRenderer.sh INSTANCES FRAMES
		
		INSTANCES ... number of instances used
		FRAMES    ... number of frames created
		
		The script generates the ini-files for povray and uploads them to the HOSTs set in the file. After that, it starts povray with them and it collects all images on the host and runs gm.
		
		
		Note: The files needed to exec myRemoteRenderer.sh can be uploaded with `upload.sh HOST`. With `ls.sh HOST` you can see all files on the hostsystem.

3. Collect the execution times of the Jobs on the selected resources and develop a scheduling that has as little load imbalance as possible (the resources have different execution speeds!). You might need to increase the number of frames to make this possible.

		karwendel.dps.uibk.ac.at 	about 36-38 sec for 1 image 10 times
		login.leo1.uibk.ac.at		-
		lise.jku.austriangrid.at	about 16-17 sec for 1 image 10 times
		
		look at createIniMeta.sh
		
		In createIniMeta.sh the instances where splitted in half. Then we split the frames depending on how much faster host 2 is.
		
Therefore createIniMeta.sh is called as
        
         ./createIniMeta.sh #Frames #Instances #Speedup

Let's assume the speedup is 2 (as in our case for lise) then we create 2x the subset of frames for this grid site.

When we use an odd number of instances the speedup is also taken into account and we use 1 instance more of the faster type.

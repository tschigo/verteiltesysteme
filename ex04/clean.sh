#!/bin/bash

if [ "$#" -lt 1 ]
then
  echo "No host set"
  exit 1;
fi

globus-job-run $1 /bin/sh -c "rm *.ini *.png"
globus-job-run $1 /bin/sh -c "ls -l"


#!/bin/bash

if [ "$#" -lt 5 ]
then
  echo "Set all_frames, frames, instances, F_START, NAME"
  exit 1;
fi


#frames
M=$2

#instances
N=$3

# calc ini files
fpp=$((M/N))

F_START=$4

F_END=$((F_START-1))

NAME=$5

START="1"

# create ini files
echo "Creating ini files"
while [ $START -le $N ]
do
  F_START=$[F_END+1]
  if [ $((M%N)) -ge $START ]; then
    F_END=$[$F_END+$fpp+1]
  else
    F_END=$[$F_END+$fpp]
  fi

  echo "Antialias=Off" >> scherk_$NAME.ini
  echo "Antialias_Threshold=0.1" >> scherk_$NAME.ini
  echo "Antialias_Depth=2" >> scherk_$NAME.ini
  echo "Initial_Clock=0" >> scherk_$NAME.ini
  echo "Final_Clock=1" >> scherk_$NAME.ini
  echo "Cyclic_Animation=on" >> scherk_$NAME.ini
  echo "Pause_when_Done=off" >> scherk_$NAME.ini
  echo "Initial_Frame=1" >> scherk_$NAME.ini
  echo "Final_Frame="$1 >> scherk_$NAME.ini

  echo "Subset_Start_Frame="$F_START >> scherk_$NAME.ini
  echo "Subset_End_Frame="$F_END >> scherk_$NAME.ini

  START=$[$START+1]
  NAME=$[$NAME+1]
done

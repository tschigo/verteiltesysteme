#!/bin/bash

if [ "$#" -lt 3 ]
then
  echo "Set frames, instances, speedup"
  exit 1;
fi

FRAMES=$1
INST=$2
SPEED=$3

# Split Frames depending on speedup
SPLIT=$((FRAMES/(SPEED+1)))

# generate ini files, instances are split equaly
./createIni.sh $FRAMES $SPLIT $((INST/2)) 1 1
./createIni.sh $FRAMES $((FRAMES-SPLIT)) $((INST - INST/2)) $((SPLIT+1)) $((INST/2+1))

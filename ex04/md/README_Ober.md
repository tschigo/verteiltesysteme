#Verteilte Systeme++

Patrick Ober

In cooperation with Michael Tscholl and Lukas Huber

Exercises solved:

1. ✓ (1 point)
2. ✓ (4 points)
3. ✓ (5 points)

	Total: 10 points


##GRID COMPUTING (10 POINTS)


The basic steps required to use the Austrian Grid will be shown in the Lab. For this sheet you will need to render our movie on two sites of the following 3 Austrian Grid sites:

	karwendel.dps.uibk.ac.at
	login.leo1.uibk.ac.at
	lise.jku.austriangrid.at


1. **Copy all applications need to be executed on the resource: povray and gm;**
	
	The upload is implemented in `upload.sh` and the grid site must be given as a parameter
	The command `lobus-url-copy` is used to copy the needed resources onto the grid site.


2. **Write a application that can be executed on karwendel, that starts the povray movie rendering on the Grid resources and collects the result.**

	Implemented in `myRemoteRenderer.sh`.
	
		./myRemoteRenderer.sh #Instances #Frames
		#Instances	...	number of instances that should be used.
		#Frames		...	number of processors that should be used
	
	The command `globus-job-run` is used to run povray remotely.
	

3. **Collect the execution times of the Jobs on the selected resources and develop a scheduling that has as little load imbalance as possible (the resources have different execution speeds!). You might need to increase the number of frames to make this possible.**

	The balancing was calculated staticly with the povray time for one image on each grid site.
		
	On karwendel a render job took around 34 seconds and on lise around 17 seconds. Hence, on lise we can run twice the jobs as on karwendel. The distribution of render jobs is set in the .ini files created by `createIniMeta.sh` which calls `createIni.sh`. 
	
	The synchronization point is set to the last call on the 2nd host. This can be done, because `globus-job-run` is a blocking call. Because the balancing is kind of accurate, all jobs finish almost simultaneously with the blocking call. 
	
	A buffer of 10seconds is given for anyways to be a little more secure. After all pictures are collected, gm is called locally to create the gif. Afterwards, `clean.sh` is called with the Hosts to delete unnecessary files.

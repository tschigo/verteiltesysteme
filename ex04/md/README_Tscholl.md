#Verteilte Systeme++


Sheet 04, 24.04.2015

Michael Tscholl (lab619)

In cooperation with Lukas Huber and Patrick Ober
files: awsStart.sh, cloudRender.sh, showInstances.sh, exec_gm.sh, exec_job.sh

1. done		1 point
2. done 	4 point
3. done 	5 point

	 total 10 points


##GRID COMPUTING (10 POINTS)


The basic steps required to use the Austrian Grid will be shown in the Lab. For this sheet you will need to render our movie on two sites of the following 3 Austrian Grid sites:

	 - karwendel.dps.uibk.ac.at
	 - login.leo1.uibk.ac.at
	 - lise.jku.austriangrid.at

1. Copy all applications need to be executed on the resource: povray and gm;
	
		
		./upload.sh HOST
		
		HOST ... can be karwendel.dps.uibk.ac.at, login.leo1.uibk.ac.at or lise.jku.austriangrid.at
		
		The script uploads all files needed for povray and gm and makes povray and gm executable

2. Write a application that can be executed on karwendel, that starts the povray movie rendering on the Grid resources and collects the result.

		./myRemoteRenderer.sh INSTANCES FRAMES
		
		INSTANCES ... number of instances used
		FRAMES    ... number of frames created
		
		The scirpt generates the ini-files for povray and uploads them to the HOSTs set in the file. After that, it starts povray with them and it collects all images on the host and runs gm.
		
		
		Note: The files needed to exec myRemoteRenderer.sh can be uploaded with `upload.sh HOST`. With `ls.sh HOST` you can see all files on the hostsystem.

3. Collect the execution times of the Jobs on the selected resources and develop a scheduling that has as little load imbalance as possible (the resources have different execution speeds!). You might need to increase the number of frames to make this possible.

		karwendel.dps.uibk.ac.at 	about 36-38 sec for 1 image 10 times
		login.leo1.uibk.ac.at		-
		lise.jku.austriangrid.at	about 16-17 sec for 1 image 10 times
		
		look at createIniMeta.sh
		
		In createIniMeta.sh the instances where splitet in half. Then we split the frames depending on how much faster host 2 is.
#!/bin/bash

if [ "$#" -ne 2 ]
then
  echo "processors / frames missing!"
  exit 1
fi

HOST_1="karwendel.dps.uibk.ac.at"
HOST_2="lise.jku.austriangrid.at"

INSTANCES=$1 #number of instances
FRAMES=$2 #number of frames

# create ini files
./createIniMeta.sh $FRAMES $INSTANCES 2

#####################

# upload ini files to host 1
i="1"
while [ $i -le $((INSTANCES/2)) ]
do
  globus-url-copy scherk_$i.ini gsiftp://$HOST_1/~/
  echo "upload scherk_$i to $HOST_1"
  i=$[$i+1]
done

# upload ini files to host 2
i=$((INSTANCES/2+1))
while [ $i -le $INSTANCES ]
do
  globus-url-copy scherk_$i.ini gsiftp://$HOST_2/~/
  echo "upload scherk_$i to $HOST_2"
  i=$[$i+1]
done

#####################

# start jobs on host 1
i="1"
while [ $i -le $((INSTANCES/2)) ]
do
 globus-job-run $HOST_1 ./povray scherk_$i.ini +Ischerk.pov +Oscherk.png +FN +W1024 +H768 &
  echo "job $i on $HOST_1"
  i=$[$i+1]
done

# start jobs on host 2
i=$((INSTANCES/2+1))
while [ $i -le $INSTANCES ]
do
  if [ $i -eq $INSTANCES ]; then
    globus-job-run $HOST_2 ./povray scherk_$i.ini +Ischerk.pov +Oscherk.png +FN +W1024 +H768
  else
    globus-job-run $HOST_2 ./povray scherk_$i.ini +Ischerk.pov +Oscherk.png +FN +W1024 +H768 &
  fi
  echo "job $i on $HOST_2"
  i=$[$i+1]
done

#####################

sleep 10

# collect all images
echo "collect images"
globus-url-copy gsiftp://$HOST_1/~/*.png ./
globus-url-copy gsiftp://$HOST_2/~/*.png ./

#####################

# create gif
echo "Create gif"
./Povray/gm convert -loop 0 -delay 0 *.png scherk.gif

#####################

# cleanup

rm *.ini
rm *.png
./clean.sh $HOST_1
./clean.sh $HOST_2

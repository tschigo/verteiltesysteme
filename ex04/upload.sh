#!/bin/bash

if [ "$#" -lt 1 ]
then
  echo "No host to upload set"
  exit 1;
fi


globus-url-copy Povray/gm gsiftp://$1/~/
globus-url-copy Povray/povray gsiftp://$1/~/
globus-url-copy Povray/scherk.args gsiftp://$1/~/
globus-url-copy Povray/scherk.pov gsiftp://$1/~/

globus-job-run $1 /bin/sh -c "chmod +x gm povray"
globus-job-run $1 /bin/sh -c "ls -l"


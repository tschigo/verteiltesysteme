#Verteilte Systeme++

Patrick Ober

In cooperation with Michael Tscholl and Lukas Huber

Exercises solved:

1. ✓ (15 point)

total: 15 points.

##ADVANCED PROGRAMMING


For this exercise you can choose to either write a program that is able to submit jobs to grid resources to render a movie using povray or write a program that is able to start and stop cloud resources (not using command line tools -> API tools).

If you use Java I can recommend: 

- [Grid job submissions](https://wiki.cogkit.org/wiki/Main_Page) 
- [Cloud instance creation](https://jclouds.apache.org/start/install)

The program can reuse the previews generated tools but is not allowed to use command line tools for some parts of the communication! The describing document should highlight if you choose Grid or Cloud, what programming language and what library you used for the solution!



###GRID:
The job submission and job monitoring should be done using the Java COG Kit or any other comparable libraries in any programming language of your choice. You will need to include some security mechanisms (Grid Proxy usage) in your program. The program should submit multiple jobs to 2+ Austrian Grid sites and collect the resulting animated GIF. File transfers can still be done using globus-url-copy but additional 5 points are awarded if this is done inside of your program as well (i.e. using griftp).


###CLOUD:
The creation of instances and termination should be done using the JCloud library or any other comparable libraries in any programming language of your choice. You will need to include some security mechanisms to authenticate with EC2. The program should submit multiple jobs to 2+ Cloud instances and collect the resulting animated GIF followed by a termination of the instances. Job submissions can still be done using SSH but additional 5 points are awarded if this is done inside of your program as well (i.e. using some ssh library).


####This solution uses jClouds, jsch, ec2 instances and s3 buckets.
	
Basic overview:

	The application offers a simple user interface on the command line. 
	It is possible to create a certain number of instances and run povray/gm on it with a given number of frames. This happens via ssh (jsch, with temporary keys) executing calls to scripts which are located on the instance image. 
	The instances are mounting the same s3 bucket and use that to store the png files onto it. Gm also works on the s3 bucket and stores the gif there, aswell. The user interface offers a download to get the generated gif.
	Also a termination of all instances is possible.
	

How to create a jar:

	- Eclipse
		import as an existing maven projekt
		
		- right-click project
		- run as
		- run configurations..
		- double click maven build (to create a new configuration)
		- give a name for configuration e.g. package
		- click variables
		- select "selected_resource_loc" and click ok
		- write your goal e.g. "package" or "clean package"
		- run


	- maven
		> cd ex05
		> mvn package

	jar and dependencies will be saved in target folder.


How to run jar:
	
		>java -jar target/awsRenderer.jar

	

###Useful links:
- https://wiki.cogkit.org/wiki/Java_Cog_Kit_Examples_Guide 
- http://jclouds.apache.org/start/compute/ 
- http://jclouds.apache.org/guides/aws-ec2/

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.BlobStoreContext;
import org.jclouds.blobstore.domain.Blob;
import org.jclouds.compute.ComputeService;
import org.jclouds.compute.ComputeServiceContext;
import org.jclouds.compute.RunNodesException;
import org.jclouds.compute.domain.NodeMetadata;
import org.jclouds.compute.domain.OsFamily;
import org.jclouds.compute.domain.Template;
import org.jclouds.ec2.domain.InstanceType;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class InstanceManager {

    private String aws_access_key_id = null;
    private String aws_secret_access_key = null;
    private ComputeService computeService = null;
    private Template template = null;
    private List<NodeMetadata> nodes = null;

    private String key = "/tmp/jclouds.pem";

    public InstanceManager(String aws_access_key_id,
                           String aws_secret_access_key) {
        this.nodes = new ArrayList<>();
        this.aws_access_key_id = aws_access_key_id;
        this.aws_secret_access_key = aws_secret_access_key;
        removeOldKey();
        createComputeService();
        createTemplate();
    }

    /**
     * Create template which defines instance settings.
     */
    private void createTemplate() {
        this.template = this.computeService.templateBuilder()
                .hardwareId(InstanceType.T2_MICRO)
                .imageId("us-west-1/ami-1bc12e5f")
                .osFamily(OsFamily.AMZN_LINUX).locationId("us-west-1")
                .build();
    }

    /**
     * Create compute service to handle aws actions
     */
    private void createComputeService() {
        this.computeService = ContextBuilder.newBuilder("aws-ec2")
                .credentials(aws_access_key_id, aws_secret_access_key)
                .buildView(ComputeServiceContext.class).getComputeService();
    }

    /**
     * Removes generated access key to instances
     */
    private void removeOldKey() {

        try {
            Runtime.getRuntime().exec("rm -rf " + this.key);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create file. Used when keys are generated.
     *
     * @param canonicalFilename path and filename
     * @param text              saved in the file
     */
    private void writeFile(String canonicalFilename, String text) {
        try {
            PrintWriter out = new PrintWriter(canonicalFilename);
            out.write(text);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * Run command on node via ssh. Temporary key is used to connect.
     *
     * @param node on which the command is executed
     * @param cmd  to execute on node
     */
    private void runSSH(NodeMetadata node, String cmd) {
        //     this.computeService.getContext().utils().sshForNode().apply(this.nodes.get(0)).exec()
        if (!new File(this.key).exists()) {
            writeFile(this.key, node.getCredentials().getOptionalPrivateKey()
                    .or("key"));

            try {
                Runtime.getRuntime().exec("chmod 400 " + this.key);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String host = node.getPublicAddresses().iterator().next();

        JSch jsch = new JSch();
        try {
            jsch.addIdentity(this.key);
            Session session = jsch.getSession("ec2-user", host, 22);
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            System.out.println("connected to: " + host);

            ChannelExec channel = (ChannelExec) session.openChannel("exec");
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    channel.getInputStream()));
            channel.setCommand(cmd);
            channel.connect();

            String msg;
            try {
                while ((msg = in.readLine()) != null) {
                    System.out.println(msg);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("I did something ;-D");
            channel.disconnect();
            session.disconnect();
            System.out.println("session closed");
        } catch (JSchException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Download file from bucket.
     *
     * @param bucket name of the bucket
     * @param from   file that will be downloaded
     * @param to     destination file name
     */
    private void downloadFile(String bucket, String from, String to) {
        BlobStoreContext context = ContextBuilder.newBuilder("aws-s3")
                .credentials(aws_access_key_id, aws_secret_access_key)
                .buildView(BlobStoreContext.class);

        // Access the BlobStore
        BlobStore blobStore = context.getBlobStore();
        Blob fileBlob = blobStore.getBlob(bucket, from);

        InputStream in = null;
        OutputStream out = null;
        try {
            in = fileBlob.getPayload().openStream();
            out = new FileOutputStream(to);

            IOUtils.copy(in, out);
            blobStore.removeBlob(bucket, from);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                assert out != null;
                out.close();
                assert in != null;
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        context.close();
    }

    /**
     * Create instances.
     *
     * @param count number of instances to create
     */
    public void create(Integer count) {
        try {
            Set<? extends NodeMetadata> nodes = this.computeService
                    .createNodesInGroup("ex05", count, this.template);

            for (NodeMetadata node : nodes) {
                this.nodes.add(node);

            }

            // instance state: running does not mean that sshd is running...
            // wait 30 seconds
            Thread.sleep(30000);

        } catch (RunNodesException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Destroy previously created instances.
     */
    public void destroy() {
        for (NodeMetadata node : this.nodes) {
            this.computeService.destroyNode(node.getId());
        }
        removeOldKey();
        this.computeService.getContext().close();
    }

    /**
     * Run Povray. Jobs will be distributed over all previously created
     * instances in parallel.
     *
     * @param frames number of frames to render
     */
    public void runPovray(final int frames) {
        int j = 1;
        for (final NodeMetadata node : this.nodes) {
            final int i = j;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    runSSH(node,
                            "./createIni.sh " + frames + " " + nodes.size()
                                    + ";./exec_povray.sh " + i);
                }
            }).start();
            j++;
        }
    }

    /**
     * Run gm on given node. Also waits until #frames png files are on the
     * bucket. After execution, png files are removed.
     *
     * @param node   to run gm onto
     * @param frames number of png files that have to exist before execution
     */
    public void runGm(NodeMetadata node, int frames) {
        runSSH(node, "./exec_gm.sh " + frames);
    }

    /**
     * Downloads scherk.gif to given path
     *
     * @param path where scherk.gif will be downloaded to
     */
    public void downloadGif(String path) {
        downloadFile("uibk-jclouds", "scherk.gif", path);
    }

    /**
     * Get created nodes.
     *
     * @return list of created nodes
     */
    public List<NodeMetadata> getNodes() {
        return this.nodes;
    }

}


import org.jclouds.compute.domain.NodeMetadata;

import java.io.*;

public class MainApp {



    public static void main(String[] args) {
        run();
        System.out.println("finished.");
    }


    public static void run() {
        System.out.println("initializing instance manager...");
        Boolean running = true;

        InstanceManager instanceManager = new InstanceManager("AKIAIYGB5RB7IKP6LFNQ", "fvWbK9DgBYPkSPMRHCr6FkLpIHJJ7Cq5lp8we4hK");


        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(
                System.in));
        System.out.println("Press key for action");
        System.out.println("h: help\n");
        System.out.println("c: create Instances (eg. c 4)");
        System.out.println("i: show Information");
        System.out.println("r: render gif, set frames (eg. r 100)");
        System.out.println("d: download gif, set destination");
        System.out.println("q: destroy instances and quit");

        try {
            while (running) {
                System.out.print(">");
                String s = bufferRead.readLine();


                switch (s.charAt(0)) {
                    case 'h':
                        System.out.println("Press key for action");
                        System.out.println("h: help\n");
                        System.out.println("c: create Instances (eg. c 4)");
                        System.out.println("i: show Information");
                        System.out.println("r: render gif, set frames (eg. r 100)");
                        System.out.println("d: download gif, set destination");
                        System.out.println("q: destroy instances and quit");
                        break;

                    case 'c':
                        int i = 0;
                        if (s.length() <= 2) {
                            i = 1;
                        } else if (s.length() > 2) {
                            i = Integer.parseInt(s.substring(2));
                        }
                        System.out.println("creating " + i + " instances...");
                        instanceManager.create(i);
                        System.out.println("done.");
                        break;

                    case 'q':
                        System.out.println("destroying all instances...");
                        instanceManager.destroy();
                        running = false;
                        System.out.println("all instances are closed.");
                        break;

                    case 'i':
                        if (instanceManager.getNodes().isEmpty()) {
                            System.out.println("no running instance found");
                        } else {
                            for (NodeMetadata node : instanceManager.getNodes()) {
                                System.out.println("hostname: " + node.getId() + "\t ip:" + node.getPublicAddresses());
                            }
                        }
                        break;

                    case 'r': // set frames
                        if (instanceManager.getNodes().isEmpty()) {
                            System.out.println("no running instance found");
                            break;
                        }
                        int c;
                        try {
                            c = Integer.parseInt(s.substring(2));
                        } catch (NumberFormatException | StringIndexOutOfBoundsException e) {
                            c = 10;
                        }
                        System.out.println("starting povray and gm ...");
                        instanceManager.runPovray(c);
                        instanceManager.runGm(instanceManager.getNodes().get(0), c);
                        System.out.println("done.");
                        break;

                    case 'd':
                        System.out.println("downloading gif...");
                        String path = "scherk.gif";
                        if (s.length() != 1) {
                            path = s.substring(2);
                        }
                        instanceManager.downloadGif(path);
                        System.out.println("done.");
                        break;

                    default:
                        System.out.println("default");
                        break;
                }
            }
            System.out.println("bye!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
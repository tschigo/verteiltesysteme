import org.cogkit.abstraction.impl.common.AbstractionFactory;
import org.cogkit.abstraction.impl.common.ProviderMethodException;
import org.cogkit.abstraction.impl.common.StatusEvent;
import org.cogkit.abstraction.impl.common.task.*;
import org.cogkit.abstraction.interfaces.*;


/*
 * useful link Java Cog Kit Examples Guide
 * @link https://wiki.cogkit.org/wiki/Java_Cog_Kit_Examples_Guide
 * @link http://www.cogkit.org/release/current/api/abstraction-common/index.html
 */
public class CoG {


    enum MODE {
        LOCAL_UPLOAD, LOCAL_DOWNLOAD, TRANSFER
    }


    public void fileTransfer(String taskname, MODE mode, String from_host, String from_file, String to_host, String to_file) {
        try {
            FileTransferSpecificationImpl spec = new FileTransferSpecificationImpl();
            spec.setDestinationFile(to_file);
            spec.setSourceFile(from_file);

            Task t = new TaskImpl(taskname, Task.FILE_TRANSFER);
            switch (mode) {
                case LOCAL_UPLOAD:
                    t.setService(0, new ServiceImpl("local", new ServiceContactImpl("localhost"), null));
                    t.setService(1, new ServiceImpl("gsiftp", new ServiceContactImpl(to_host), null));
                    break;
                case LOCAL_DOWNLOAD:
                    t.setService(0, new ServiceImpl("gsiftp", new ServiceContactImpl(from_host), null));
                    t.setService(1, new ServiceImpl("local", new ServiceContactImpl("localhost"), null));
                    break;
                case TRANSFER:
                    t.setService(0, new ServiceImpl("gsiftp", new ServiceContactImpl(from_host), null));
                    t.setService(1, new ServiceImpl("gsiftp", new ServiceContactImpl(to_host), null));
                    break;
                default:
                    System.out.println("Unknown MODE");
                    return;
            }

            t.setSpecification(spec);
            t.addStatusListener(new jobStatusListener());
            AbstractionFactory.newFileTransferTaskHandler().submit(t);
            t.waitFor();
            System.out.println("Done");
        } catch (ProviderMethodException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (InvalidProviderException e) {
            e.printStackTrace();
        } catch (IllegalSpecException e) {
            e.printStackTrace();
        } catch (InvalidSecurityContextException e) {
            e.printStackTrace();
        } catch (TaskSubmissionException e) {
            e.printStackTrace();
        } catch (InvalidServiceContactException e) {
            e.printStackTrace();
        }
    }

    public void run(final String executable, final String taskname) {
        run(executable, taskname, "local");
    }

    public void run(final String executable, final String taskname, String hostname) {
        try {

            Specification specification = new JobSpecificationImpl();
            specification.setType(Specification.JOB_SUBMISSION);
            specification.setSpecification("test");

            Task task = new TaskImpl("/bin/hostname", Task.JOB_SUBMISSION);
            task.setSpecification(specification);


            task.setService(Service.JOB_SUBMISSION, new ServiceImpl("local", new ServiceContactImpl("localhost"), null));

            task.addStatusListener(new jobStatusListener());
            AbstractionFactory.newExecutionTaskHandler("local").submit(task);
            Thread.sleep(2000);
        } catch (ProviderMethodException e) {
            e.printStackTrace();
        } catch (InvalidProviderException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (InvalidSecurityContextException e) {
            e.printStackTrace();
        } catch (InvalidServiceContactException e) {
            e.printStackTrace();
        } catch (IllegalSpecException e) {
            e.printStackTrace();
        } catch (TaskSubmissionException e) {
            e.printStackTrace();
        }
    }

    // status listener
    private static class jobStatusListener implements StatusListener {
        public void statusChanged(StatusEvent event) {
            System.out.println(event.getStatus().toString());
            if (event.getStatus().getStatusCode() == Status.COMPLETED) {
                System.out.println(((Task) event.getSource()).getStdOutput());
            }
        }
    }


    //TODO impl later
    public void uploadFiles(String host) {
    }

    public void runPovray(String... hosts) {
        // create on every host ini-Files
        // run povray
        // collect images
    }

    public void runGm(String host) {
        // run gm
    }

    public void downloadGif() {
        // download gif
    }
}

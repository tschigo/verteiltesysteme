import java.util.Scanner;

/**
 * karwendel.dps.uibk.ac.at
 * login.leo1.uibk.ac.at
 * lise.jku.austriangrid.at
 */
public class MainApp {

    public static void main(String[] args) {
        ui();
    }

    public static void ui() {
        Boolean live = true;
        Scanner scanner = new Scanner(System.in);
        jGlobusManager manager = null;
        String cmd;
        System.out.println("==================UI jGlobusManager==================");
        while (live) {
            System.out.print(">");
            cmd = scanner.nextLine();
            if (cmd.charAt(0) == 'c') {
                try {
                    cmd = cmd.substring(2);
                    manager = new jGlobusManager(cmd.substring(0, cmd.indexOf(',')), cmd.substring(cmd.indexOf(',') + 1, cmd.length()));
                } catch (Exception e) {
                    manager = null;
                    System.out.println("Could not create jGlobusManager");
                }
            } else if (cmd.charAt(0) == 'q') {
                live = false;
                System.out.println("ciao");
            } else if (cmd.charAt(0) == 'd') {
                manager = null;
                System.out.println("jGlobusManager destroyed");
            } else if (cmd.charAt(0) == 'r') {
                if (manager != null) {
                    System.out.println("There is any jGlobusManager");
                } else {
                    manager.povray();
                    manager.gm();
                }
            } else if (cmd.charAt(0) == 'h') {
                System.out.println("h ... help");
                System.out.println("c ... create jGlobusManager");
                System.out.println("q ... quit");
                System.out.println("d ... destroy jGlobusManager");
                System.out.println("r ... run povray and gm");
            } else {
                System.out.println("Unknown command");
            }
        }
    }

}

import org.apache.commons.io.FileUtils;
import org.globus.gsi.gssapi.auth.Authorization;
import org.globus.myproxy.MyProxy;
import org.globus.util.ConfigUtil;
import org.gridforum.jgss.ExtendedGSSCredential;
import org.gridforum.jgss.ExtendedGSSManager;
import org.ietf.jgss.GSSCredential;
import org.ietf.jgss.GSSException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class jGlobusManager {

    private List<MyProxy> hosts;


    enum FILE_TRANSFER {
        UPLOAD,
        DOWNLOAD
    }

    public jGlobusManager(String host1, String host2) {
        this.hosts = new ArrayList<>();
        this.hosts.add(createProxy(host1));
        this.hosts.add(createProxy(host2));
    }

    private MyProxy createProxy(String host) {
        MyProxy proxy = new MyProxy();
        proxy.setHost(host);
        Authorization auth = null;
        getDefaultCredential();
        proxy.setAuthorization(auth);
        return proxy;
    }

    private GSSCredential getDefaultCredential() {
        try {
            String proxy = ConfigUtil.discoverProxyLocation();
            byte[] bytes = FileUtils.readFileToByteArray(new File(proxy));
            return ((ExtendedGSSManager) ExtendedGSSManager.getInstance())
                    .createCredential(bytes, ExtendedGSSCredential.IMPEXP_OPAQUE,
                            GSSCredential.DEFAULT_LIFETIME, null,
                            GSSCredential.INITIATE_AND_ACCEPT);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GSSException e) {
            e.printStackTrace();
        }

        return null;
    }


    private void fileTransfer(MyProxy proxy, File file, FILE_TRANSFER transfer) {
        switch (transfer) {
            case DOWNLOAD:
                System.out.println("Download");
                break;
            case UPLOAD:
                System.out.println("Upload");
                break;
            default:
                System.err.println("Don't know this option");
                break;
        }
    }

    public void upload() {
        for (MyProxy p : this.hosts) {
            fileTransfer(p, new File("main/resources/createIni.sh"), FILE_TRANSFER.UPLOAD);
            fileTransfer(p, new File("main/resources/povray"), FILE_TRANSFER.UPLOAD);
            fileTransfer(p, new File("main/resources/gm"), FILE_TRANSFER.UPLOAD);
            fileTransfer(p, new File("main/resources/scherk.args"), FILE_TRANSFER.UPLOAD);
            fileTransfer(p, new File("main/resources/scherk.pov"), FILE_TRANSFER.UPLOAD);
        }
    }


    private void run(MyProxy myProxy, String cmd) {
        System.out.println("[exec]\t" + myProxy.toString() + "\t" + cmd);
    }

    public void povray() {
        int i = 1;
        for (MyProxy p : this.hosts) {
            run(p, "povray " + this.hosts.size() + " " + i++);
        }
    }

    public void gm() {
        run(this.hosts.get(0), "gm");
    }

}

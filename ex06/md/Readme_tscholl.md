#Verteilte Systeme++


Sheet 06, 22.05.2015

Michael Tscholl (lab619)

In cooperation with Lukas Huber and Patrick Ober

	 total 5 points 
	 because I'm able to transfere files in my program(CoG-Project)
	 
	 
##ADVANCED PROGRAMMINGFor this exercise you can choose to either write a program that is able to submit jobs to grid resources to render a movie using povray or write a program that is able to start and stop cloud resources (not using command line tools -> API tools).
If you use Java I can recommend:- [Grid job submissions](https://wiki.cogkit.org/wiki/Main_Page)The program can reuse the previews generated tools but is not allowed to use command line tools for some parts of the communication! The describing document should highlight if you choose Grid or Cloud, what programming language and what library you used for the solution!###GRID:The job submission and job monitoring should be done using the Java COG Kit or any other comparable libraries in any programming language of your choice. You will need to include some security mechanisms (Grid Proxy usage) in your program. The program should submit multiple jobs to 2+ Austrian Grid sites and collect the resulting animated GIF. File transfers can still be done using globus-url-copy but additional 5 points are awarded if this is done inside of your program as well (i.e. using griftp).		The are 2 maven project:			- CoG-Project
			- jGlobus-Project				1. CoG-Project FAILED			I started with CoG kit example Guide(there is a link at the end), where the importen stuff was missing! I was able to execute commands localy and transfer files, but I was not able to run a job on krawendel or leo1. The Javadoc documentation was useless there were no additional information.
		1.1. Java CoG Kit Karajan FAILED			Karajan is a parallel scripting language with a good tutorial.
			
			PROBLEM: 	<include file="cogkit.xml"/>				all files have a reference to cogkit.xml but I was not able to find it.
		2. jGlobus-Project FAILED			- There were multiple errors in the API!!! 			- no documentation!!!### Links of CoG:- https://wiki.cogkit.org/wiki/Java_Cog_Kit_Examples_Guide- https://wiki.cogkit.org/wiki/FAQ- https://wiki.cogkit.org/wiki/MavenRepository
- http://www.cogkit.org/release/4_1_3/api/jglobus/index.html
- https://wiki.cogkit.org/wiki/Java_CoG_Kit_Workflow_Guide
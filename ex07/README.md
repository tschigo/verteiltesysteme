###AMAZON FLOW FRAMEWORK (10 POINTS)

Make sure you do not exceed your EC2 / S3 / Flow account with the 100$ loaded from the exercise voucher! Terminate all instances after working on this sheet. Files might be kept on S3 till the end of the semester. Flow usage is not covered with the 100$ so make sure you do not 2. exceed the free tier limits!

1. Check out the example codes given for the Amazon Flow Framework. Please read the introduction on the page:
	- http://docs.aws.amazon.com/amazonswf/latest/awsflowguide/welcome.html

		And then try and run 2 of the example workflows from the sample file given under
	- Samples: http://aws.amazon.com/code/3015904745387737

		Document the steps required to execute those example files and which examples you have chosen.



2. Write your own Amazon Flow Workflow application that does the following calculation using at least 3 cloud instances:
	- Estimating the number Pi (π)

	Each Task of the workflow should calculate parts of this sum. The total amount of activities should exceed the number of used workers (10+). The resulting sum up to 4/10001 should be calculated and the resulting value stored (Check that the result is correctly!).

	Implementation details can be chosen freely as long as the calculation is done using multiple jobs (20+) in the flow framework. Usage of Java for the execution should make things easier (see examples from Ex. 1)!
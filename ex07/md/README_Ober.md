#Verteilte Systeme++

Patrick Ober

In cooperation with Michael Tscholl and Lukas Huber

Exercises solved:

1. ✓ (10 point)

total: 10 points.


###AMAZON FLOW FRAMEWORK (10 POINTS)

Make sure you do not exceed your EC2 / S3 / Flow account with the 100$ loaded from the exercise voucher! Terminate all instances after working on this sheet. Files might be kept on S3 till the end of the semester. Flow usage is not covered with the 100$ so make sure you do not 2. exceed the free tier limits!

1. Check out the example codes given for the Amazon Flow Framework. Please read the introduction on the page:
	- http://docs.aws.amazon.com/amazonswf/latest/awsflowguide/welcome.html

		And then try and run 2 of the example workflows from the sample file given under
	- Samples: http://aws.amazon.com/code/3015904745387737

		Document the steps required to execute those example files and which examples you have chosen.
				
		Follow the steps shown in the AWS guide:
		
		Setting up AWS Flow Framework for Java with the AWS Toolkit for Eclipse.
				http://docs.aws.amazon.com/amazonswf/latest/awsflowguide/setup.html
		
		1. Install AWS Toolkit for Eclipse 
				http://docs.aws.amazon.com/amazonswf/latest/awsflowguide/setup.html
		2. Download samples and unzip them
		3. Include AwsFlowFramework example with ant
		4. Download Log4j (version 1.2.15) and org.springframework.test (version 3.0) and add them to the classpath
		5. Enable Annotation processing (right click Project or cmd + I, Java Compiler,  Annotation processing)
		6. Convert the project to a "AspectJ Project" (right click on Project, Configure, Convert to AspectJ Project)
				
		Now you have to create a SWF Domain:
		
		Log into the AWS Management Console and select the Amazon SWF service.
		
		Click Manage Domains in the upper right corner and register a new Amazon SWF domain. A domain is a logical container for your application resources, such as workflow and activity types, and workflow executions. You can use any convenient domain name.
				
		To start an example, first run the ActivityHost which holds the methods for execution steps.
		Then run the WorkflowHost which distributes workers to execute the steps defined in the workflow.
		At last, run the Starter which initiates the whole process.
				
				
2. Write your own Amazon Flow Workflow application that does the following calculation using at least 3 cloud instances:

	Each Task of the workflow should calculate parts of this sum. The total amount of activities should exceed the number of used workers (10+). The resulting sum up to 4/10001 should be calculated and the resulting value stored (Check that the result is correctly!).

	Implementation details can be chosen freely as long as the calculation is done using multiple jobs (20+) in the flow framework. Usage of Java for the execution should make things easier (see examples from Ex. 1)!
	
	To run the program, follow the steps described above and set your AWS credentials.			

	The application sets up a workflow (WorkflowHost) with n steps to make a distributed and parallel calculation of π.
				After completing the calculation, the next step in the workflow is, to merge the partitioned results and print the solution.
				The ActivityHost provides the methods used to process the steps described in the workflow.
				With the ExecutionStarter the process gets initiated, also the number of chunks/workers is defined there.
				
	It is possible to run several ActivityHosts. I did this on 3 EC2 instances. SWF distributes and schedules the workers on these Hosts. It is possible to do the same for the WorkflowHost, but in this example it is running locally. Sadly i couldnt manage to create a working JAR file for the WorkflowHost, for whatever reason....
				

#Verteilte Systeme++


Sheet 07, 29.05.2015

Michael Tscholl (lab619)

In cooperation with Lukas Huber and Patrick Ober

total 10 Points

###AMAZON FLOW FRAMEWORK (10 POINTS)

Make sure you do not exceed your EC2 / S3 / Flow account with the 100$ loaded from the exercise voucher! Terminate all instances after working on this sheet. Files might be kept on S3 till the end of the semester. Flow usage is not covered with the 100$ so make sure you do not 2. exceed the free tier limits!

1. Check out the example codes given for the Amazon Flow Framework. Please read the introduction on the page:
	- http://docs.aws.amazon.com/amazonswf/latest/awsflowguide/welcome.html

		And then try and run 2 of the example workflows from the sample file given under
	- Samples: http://aws.amazon.com/code/3015904745387737

		Document the steps required to execute those example files and which examples you have chosen.
		
				1. Install AWS Toolkit for Eclipse 
					http://docs.aws.amazon.com/amazonswf/latest/awsflowguide/setup.html
				2. Download samples and unzip it
				3. Include AwsFlowFramework example with ant
				4. Download Log4j (version 1.2.15) and org.springframework.test (version 3.0) and add them to the classpath
				5. Enable Annotation processing (right click Project or cmd + I, Java Compiler,  Annotation processing)
				6. Convert the project to a "AspectJ Project" (right click on Project, Configure, Convert to AspectJ Project)
				7.  Create the Samples domain
    				1.  Go to the SWF Management Console (https://console.aws.amazon.com/swf/home).
    				2.  Follow the on-screen instructions to log in.
    				3.  Click Manage Domains and register a new domain with the name Samples.

				8.  Open the access.properties in the samples folder and add your data.


		   *Hello World Sample:*

     		The sample has three executables. You should run each in a separate terminal/console.
     		- Run: `ant -f build.xml -Dmain-class="com.amazonaws.services.simpleworkflow.flow.examples.helloworld.ActivityHost" run`
     		- Run: `ant -f build.xml -Dmain-class="com.amazonaws.services.simpleworkflow.flow.examples.helloworld.WorkflowHost" run`
     		- Run: `ant -f build.xml -Dmain-class="com.amazonaws.services.simpleworkflow.flow.examples.helloworld.WorkflowExecutionStarter" run`


   			*Booking Sample:*

			The sample has three executables. You should run each in a separate terminal/console. From the samples folder,
     		- Run: `ant -f build.xml -Dmain-class="com.amazonaws.services.simpleworkflow.flow.examples.booking.ActivityHost" run`
     		- Run: `ant -f build.xml -Dmain-class="com.amazonaws.services.simpleworkflow.flow.examples.booking.WorkflowHost" run`
     		- Run: `ant -f build.xml -Dmain-class="com.amazonaws.services.simpleworkflow.flow.examples.booking.WorkflowExecutionStarter" run`

2. Write your own Amazon Flow Workflow application that does the following calculation using at least 3 cloud instances:
	- Estimating the number Pi (π)

	Each Task of the workflow should calculate parts of this sum. The total amount of activities should exceed the number of used workers (10+). The resulting sum up to 4/10001 should be calculated and the resulting value stored (Check that the result is correctly!).

	Implementation details can be chosen freely as long as the calculation is done using multiple jobs (20+) in the flow framework. Usage of Java for the execution should make things easier (see examples from Ex. 1)!
	
		Import swf-example folder as a Eclipse project.
		create 3 jar's ActivityHost, WorkflowHost and WorkflowExecutionStarter and adapt the credentials.
import java.math.BigDecimal;

public class Test {

  public static BigDecimal piStep(int start, int end){
     BigDecimal res = new BigDecimal(0.0);

    // start fix
    start += (start % 2 == 0) ? 1 : 0;

    for(int i = start; i <= end; i += 2 ){
      BigDecimal t = new BigDecimal(4.0 / i);
      res = ((((i+1)/2) % 2) != 0) ? res.add(t) : res.subtract(t);
    }
    return res;
  }


  /**
   * M ... size
   * N ... workers
   **/
  public static BigDecimal pi (int M, int N){
    BigDecimal res = new BigDecimal(0.0);
    int r = M/N;
    int start, end = 0;
    
    for (int i = 0; i < N; i++){
      start =  end + 1;
      end += r += ((M%N) > i) ? 1 : 0;
      res = res.add(piStep(start, end));
    }
    return res;
  }

  public static void main(String[] args){
    System.out.println("pi " + pi(Integer.parseInt(args[0]), Integer.parseInt(args[1])));
  }
}

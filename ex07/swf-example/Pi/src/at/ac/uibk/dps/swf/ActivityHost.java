package at.ac.uibk.dps.swf;

import com.amazonaws.services.simpleworkflow.flow.ActivityWorker;

public class ActivityHost {

	public static void main(String[] args) throws InstantiationException, IllegalAccessException, SecurityException, NoSuchMethodException {
		
	     ServiceHelper service = new ServiceHelper();
	     String taskListToPoll = "PiActivity";

	     ActivityWorker aw = new ActivityWorker(service.getSWFservice(), ServiceHelper.DOMAIN, taskListToPoll);
	     aw.addActivitiesImplementation(new PiActivitiesImpl());
	     aw.start();

	}

}

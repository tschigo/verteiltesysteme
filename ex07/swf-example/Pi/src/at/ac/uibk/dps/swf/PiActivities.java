package at.ac.uibk.dps.swf;


import java.math.BigDecimal;

import com.amazonaws.services.simpleworkflow.flow.annotations.Activities;
import com.amazonaws.services.simpleworkflow.flow.annotations.ActivityRegistrationOptions;

@ActivityRegistrationOptions(defaultTaskScheduleToStartTimeoutSeconds = 300, defaultTaskStartToCloseTimeoutSeconds = 30)
@Activities(version = "1.0")
public interface PiActivities {
	public BigDecimal calcPart(int start, int size);
	public void printPi(BigDecimal pi);

}

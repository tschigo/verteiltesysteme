package at.ac.uibk.dps.swf;

import java.math.BigDecimal;

public class PiActivitiesImpl implements PiActivities {

	@Override
	public BigDecimal calcPart(int start, int end) {
		BigDecimal partOfPi = new BigDecimal(0.0);
		start += (start % 2 == 0) ? 1 : 0;
		System.out.println("start: " + start + ", end: " + end);
		for (int i = start; i <= end; i += 2) {
			BigDecimal tmpPi = new BigDecimal(4.0 / i);
			if (i % 4 == 3) {
				partOfPi = partOfPi.subtract(tmpPi);
			} else {
				partOfPi = partOfPi.add(tmpPi);
			}
		}
		return partOfPi;
	}

	@Override
	public void printPi(BigDecimal pi) {
		System.out.println("Pi is: " + pi);
	}
}

package at.ac.uibk.dps.swf;

import com.amazonaws.services.simpleworkflow.flow.annotations.Execute;
import com.amazonaws.services.simpleworkflow.flow.annotations.Workflow;
import com.amazonaws.services.simpleworkflow.flow.annotations.WorkflowRegistrationOptions;

@Workflow
@WorkflowRegistrationOptions(defaultExecutionStartToCloseTimeoutSeconds = 300,
defaultTaskStartToCloseTimeoutSeconds = 10)
public interface PiWorkflow {
	@Execute(version = "1.0")
	public void calcPi(int maxDivisor, int numberOfChunks);
	
}

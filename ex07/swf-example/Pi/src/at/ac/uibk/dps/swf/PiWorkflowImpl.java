package at.ac.uibk.dps.swf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.simpleworkflow.flow.annotations.Asynchronous;
import com.amazonaws.services.simpleworkflow.flow.annotations.Wait;
import com.amazonaws.services.simpleworkflow.flow.core.Promise;

public class PiWorkflowImpl implements PiWorkflow {

	private PiActivitiesClient client = new PiActivitiesClientImpl();

	@Override
	public void calcPi(int maxDivisor, int numberOfChunks) {

		List<Promise<BigDecimal>> results = new ArrayList<Promise<BigDecimal>>();

		int r = maxDivisor / numberOfChunks;
		int start, end = 0;

		for (int i = 0; i < numberOfChunks; i++) {
			start = end + 1;
			end += ((maxDivisor % numberOfChunks) > i) ? 1 + r : r;
			Promise<BigDecimal> pi = client.calcPart(start, end);
			results.add(pi);
		}

		mergeResultAndComputePi(results);

	}

	@Asynchronous
	private void mergeResultAndComputePi(@Wait List<Promise<BigDecimal>> results) {
		BigDecimal pi = new BigDecimal(0.0);
		for (Promise<BigDecimal> workerSum : results) {
			pi = pi.add(workerSum.get());
		}
		// return Promise.asPromise(pi);
		System.out.println("Workflow: Pi is: " + pi);
		client.printPi(pi);

	}

}

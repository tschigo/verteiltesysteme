package at.ac.uibk.dps.swf;

import com.amazonaws.services.simpleworkflow.model.WorkflowExecution;

public class WorkflowExecutionStarter {

	public static void main(String[] args) {
		ServiceHelper service = new ServiceHelper();
		
		PiWorkflowClientExternalFactory clientFactory = new PiWorkflowClientExternalFactoryImpl(
				service.getSWFservice(), ServiceHelper.DOMAIN);
		
		PiWorkflowClientExternal workflow = clientFactory
				.getClient();
		
		workflow.calcPi(10001, 20);

		// WorkflowExecution is available after workflow creation
		WorkflowExecution workflowExecution = workflow.getWorkflowExecution();
		
		System.out.println("Started periodic workflow with workflowId=\""
				+ workflowExecution.getWorkflowId() + "\" and runId=\""
				+ workflowExecution.getRunId() + "\"");

	}

}

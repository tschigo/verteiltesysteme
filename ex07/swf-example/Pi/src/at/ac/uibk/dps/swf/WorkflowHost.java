package at.ac.uibk.dps.swf;

import com.amazonaws.services.simpleworkflow.flow.WorkflowWorker;

public class WorkflowHost {

	public static void main(String[] args) throws InstantiationException,
			IllegalAccessException, SecurityException, NoSuchMethodException {
		ServiceHelper service = new ServiceHelper();

		String taskListToPoll = "PiWorkflow";

		WorkflowWorker wfw = new WorkflowWorker(service.getSWFservice(),
				ServiceHelper.DOMAIN, taskListToPoll);
		wfw.addWorkflowImplementationType(PiWorkflowImpl.class);
		
		wfw.start();
	}

}
